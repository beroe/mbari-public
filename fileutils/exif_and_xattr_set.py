#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
#####################################################
exif_and_xattr_set.py
#####################################################
Add xattr tags and exif fields to an image file.

To run, 
Traverses all subdirs of the current directory and tags jpg or nef files with 
	- exif keywords
	- exif credit in various forms
	- xattr usertags, hopefully for future spotlight indexing
	- if the Finder creation date is bad, tries to replace with EXIF creation
	
Tags are generated from the subfolder name and the base folder name

NOTE: does not tag image files in the current directory, only in subfolders
     You can make it run on the current dir by giving . as the parameter

To run on all folders of the current directory, use:
  % exif_and_attr.py -
  
To run only on files in the current directory (not subfolders):
  % exif_and_attr.py .

To run on just particular folders of the subfolder, specify their names:
  % exif_and_attr.py Folder1 OtherFolders*

Edit the values in the initcreditstring() function to match your own info

TODO: Get GPS data from other database or file and embed

Version 1.06 - Parses files a bit differently and fixes dates
Version 1.05 - Steve Haddock - haddock at mbari dot org...

#####################################################
"""

import os, sys
import subprocess
import glob


def initcreditstring(NameString = "Steven Haddock", email = "haddock at mbari dot org"):
	CreditString = ' -credit="<{0}>" '.format(email)
	CreatorFields = "Creator,Artist,Copyright,By-line,CopyrightNotice,Rights"
	CreatorString = ""
	for Tag in CreatorFields.split(","):
		CreatorString += ' -{0}="{1}"'.format(Tag.lower(),NameString)

	CreateDict = {
	'CreatorAddress'       : 'MBARI',
	'CreatorPostalCode'    : '95039',
	'CreatorWorkURL'       : '"http://www.mbari.org/~haddock"',
	'CreatorCountry'       : 'USA',
	'CreatorWorkTelephone' : '831-775-1793'
	}
	
	for OtherTag in CreateDict:
		CreatorString += ' -{0}={1}'.format(OtherTag,CreateDict.get(OtherTag))
		
	if DEBUG: sys.stderr.write("\n## CreateStr: {}\n".format(CreditString + CreatorString))

	return CreditString + CreatorString
	


def writexattrs(F,TagList):
	""" writexattrs(F,TagList):
	Not currently used - directory-based approach below
	writes the list of tags to three xattr field on a file-by file basis:
	"kMDItemFinderComment","_kMDItemUserTags","kMDItemOMUserTags
	Redone to use subprocess instead of xattr module. slower but no dependency"""
	
	Result = ""
	# TagList.append("subprocess")
	
	plistFront = '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><array>'
	plistEnd = '</array></plist>'
	plistTagString = ''
	for Tag in TagList:
		plistTagString = plistTagString + '<string>{}</string>'.format(Tag)
	TagText = plistFront + plistTagString + plistEnd
	
	OptionalTag = "com.apple.metadata:"
	XattrList = ["kMDItemFinderComment","_kMDItemUserTags","kMDItemOMUserTags"]
	# Optional extended list of tags
	# XattrList = ["kOMUserTags","kMDItemOMUserTags","_kMDItemUserTags","kMDItemkeywords"]
	for Field in XattrList:
		# using module instead of subprocess....
		# xattr.setxattr (F,OptionalTag+Field,TagText.encode('utf8'))

		XattrCommand = 'xattr -w {0} \'{1}\' "{2}"'.format(OptionalTag + Field,TagText.encode("utf8"),F)
		if DEBUG: print XattrCommand
		#sys.stderr.write("XATTR: {}\n".format(XattrCommand))
		# ProcOut = subprocess.Popen(XattrCommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True) 
		ProcString = subprocess.check_output(XattrCommand, stderr=subprocess.STDOUT,shell=True) 
# 		print ProcString
		Result += ProcString
		#Result.append(ProcOut.stdout.readlines())		
	return Result


def	getfilecreatedate(filename):
	# use exiftool with epochsecond format
	exifcommand = 'exiftool -v0 -createdate -S  "%s" ' % (filename)

	#returns FinderCreationDate in 01/28/2007 11:05:31 format
	findercommand = 'GetFileInfo -d "%s"' %(filename)
	# print exifcommand

	fpipe = os.popen(findercommand)
	fdata = fpipe.readlines()
	
	if fdata:
		try:
			fyear = int(fdata[0].split(" ")[0].split("/")[2])
			if (fyear>1968):
				freturn = fyear
			else:
				freturn = 0
		except:
			print fdata
			freturn=0
	else:
		freturn = 0

	expipe = os.popen(exifcommand)
	exdata = expipe.readlines()
	# print "EXDATA,",exdata
	try:
		dateline = exdata[0].replace("CreateDate: ","")
		exyear = int(dateline.split(":")[0])
		if (exyear > 0):
			dates = dateline.replace(":","").replace(" ","")
			return dateline,dates,freturn
		else:
			return 0,0,freturn
	except:
		return 0,0,freturn
		
def setcreatedate(F,DateTag):
	DateList = DateTag.rstrip().split(" ")
	YearSwitch = DateList[0].split(":") # This has year first

	# Put the year last and join with /
	if int(YearSwitch[0])> 1980:
		# Put the year last and join with /
		NewString = "/".join([ YearSwitch[1],YearSwitch[2],YearSwitch[0]])
		DateString = NewString + " " + DateList[1]
		# print DateString
		sys.stderr.write(".")
		setcommand = 'SetFile -d "{0}" "{1}"'.format(DateString,F)
		ProcString = subprocess.check_output(setcommand, stderr=subprocess.STDOUT,shell=True) 
	else:
		ProcString = ""
	return ProcString


	# Only do this for old images, not new ones


def writeexifkeywords(F,KeywordList,CreditLine):
	""" This function (not used, operates on a File)
	The other function below operates on a folder instead """
	# build the keyword parameters based on this format:
	# 'exiftool  -v0 -overwrite_original -keywords=one -keywords=two -keywords=four -credit={haddock at mbari dot org}'
	# can apply to entire directory at once with "." or *.JPG
	#### NOTE, if you want to append to existing keywords or credit, use this format:
	# exiftool '-keywords<$keywords five' -m anew.jpg 
	# or this for credit: exiftool -m '-credit<$credit {haddock at mbari dot org}'
	# Creator, Artist, Copyright, By-line, CopyrightNotice,Rights
	# Testing Lightroom export with tags:
	# Keywords go under subject and under keywords?
	
	
	KeywordString = ""
	for Keyword in KeywordList:
		KeywordString += ' -keywords="{0}" -subject="{0}"'.format(Keyword[:64])
		
		
	
	# turn off backup file verbose 0  -overwrite_original 
	ExifCommand = u'exiftool  -v0 -overwrite_original {0} {1} "{2}"'.format(KeywordString,CreditLine,F)
	ExifCommand = ExifCommand.encode('utf8')# sys.stderr.write(ExifCommand + "\n")
	if DEBUG: sys.stderr.write("\n==>Exif: {0}\n".format(ExifCommand))
	
	ProcOut = subprocess.Popen(ExifCommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True) 
	Result = ProcOut.stdout.readlines()
	
	# subprocess format:
	# subprocess.Popen('ls *.jpg', stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True).stdout.readlines()


def makehexattr(CommentString):
	"""Unfinished - was looking into making a full hex comment since Finder doesn't update, but can use plist instead"""
	#SKIP THIS FUNCTION
	StartHex = "62 70 6C 69 73 74 30 30".replace(" ","")
	EndHex   = """08 00 00 00 00 00 00 01 01 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00""".replace(" ","").replace("\n","")
	FirstNum = hex(int("50",16) + len(CommentString))
	if DEBUG:
		Note = """
		START

		62 70 6C 69 73 74 30 30 51 # X 
		62 70 6C 69 73 74 30 30 52 # XX
		62 70 6C 69 73 74 30 30 53 # XX
		62 70 6C 69 73 74 30 30 5A # x10
		62 70 6C 69 73 74 30 30 5F 10 0F # 15
		62 70 6C 69 73 74 30 30 5F 10 11 # 17
		62 70 6C 69 73 74 30 30 5F 10 1B #27
		62 70 6C 69 73 74 30 30 5F 11 01 00 # 256
		62 70 6C 69 73 74 30 30 5F 10 EC # 237
		123456789062 70 6C 69 73 74 30 30 5F 10 FE # 245

		TAIL ENDS WITH NUMBER
		08 00 00 00 00 00 00 01 01 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 CA
		08 00 00 00 00 00 00 01 01 00 00 00 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 1D
		0A

		TAIL NUMBER
		0A
		0B
		0C
		01 0C 
		01 09 #245
	
		"""

def write_xattr_to_folder(D,TagList):
	"""recursive approach to xattr with -rw. will fail with error if file is locked..."""
	# PLIST approach - is necessary!
	plistFront = '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><array>'
	plistEnd = '</array></plist>'
	plistTagString = ''
	# TagList.append("reprogram")
	for Tag in TagList:
		plistTagString = plistTagString + '<string>{}</string>'.format(Tag)
	TagText = plistFront + plistTagString + plistEnd
	OptionalField = "com.apple.metadata:"
	XattrList = ["kMDItemFinderComment",
				 "kMDItemOMUserTags",
				 "_kMDItemUserTags"]
	# Optional extended list of tags
	# XattrList = ["kOMUserTags","kMDItemOMUserTags","_kMDItemUserTags","kMDItemkeywords"]
	Result = []
	for Field in XattrList:
		XattrCommand = 'xattr -rw {0} \'{1}\' "{2}"'.format(OptionalField + Field,TagText.encode("utf8"),D)
		if DEBUG: sys.stderr.write("XATTR: {}\n".format(XattrCommand))
		# ProcOut = subprocess.Popen(XattrCommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True) 
		ProcString = subprocess.check_call(XattrCommand, stderr=subprocess.STDOUT,shell=True) 
# 		print ProcString
		Result.append(ProcString)
		#Result.append(ProcOut.stdout.readlines())		
	return Result

def write_exif_to_folder(D,KeywordList,CreditLine):
		# build the keyword parameters based on this format:
		# 'exiftool  -v0 -overwrite_original -keywords=one -keywords=two -keywords=four -credit={haddock at mbari dot org}'
		# can apply to entire directory at once with "." or *.JPG
		#### NOTE, if you want to append to existing keywords or credit, use this format:
		# exiftool '-keywords<$keywords five' -m anew.jpg 
		# or this for credit: exiftool -m '-credit<$credit {haddock at mbari dot org}'
		# Creator, Artist, Copyright, By-line, CopyrightNotice,Rights
		# Testing Lightroom export with tags:
		# Keywords go under subject and under keywords?
	
	
		KeywordString = ""
		for Keyword in KeywordList:
			KeywordString += ' -keywords="{0}" -subject="{0}"'.format(Keyword[:64])
		
	
		# turn off backup file verbose 0  -overwrite_original 
		ExifCommand = u'exiftool  -v0 -overwrite_original_in_place {0} {1} "{2}"'.format(KeywordString,CreditLine,D)
		ExifCommand = ExifCommand.encode('utf8')# sys.stderr.write(ExifCommand + "\n")
		if DEBUG: sys.stderr.write("\n==>Exif: {0}\n".format(ExifCommand))
	
		ProcOut = subprocess.Popen(ExifCommand, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True) 
		Result = ProcOut.stdout.readlines()
		return Result
		# subprocess format:
		# subprocess.Popen('ls *.jpg', stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True).stdout.readlines()
		
		
DEBUG = False
DirCount = 0
FileCount = 0
XFileCount = 0
NonImageCount = 0
CHANGEDATE = True

if __name__ == "__main__": 
	# Flist = sys.argv[1:]
	BaseDir = os.getcwd()
# 	SubDirs = os.walk(BaseDir).next()[1]	# Cool tip from StackOverflow

	if len(sys.argv)<2:
			sys.exit(__doc__)
	else: # give a way to run on specified directories...
		if (sys.argv[1] == "-h" or sys.argv[1] == "--help"):
			sys.exit(__doc__)
		elif sys.argv[1] == "-":
			SubDirs = os.walk(BaseDir).next()[1]	# Cool tip from StackOverflow
		else:	
			SubDirs = sys.argv[1:]
				
	CreditLine = initcreditstring()  # defaults to my name

	BaseName = os.path.split(BaseDir)[-1]
	
	if DEBUG: sys.stderr.write("BaseDir: {0}\n".format(BaseName))

	# Use this method if we are going to use the file lists later, but apparently not...
	# AllFiles = [x for x in os.walk(BaseDir)]
	# SubDirs = AllFiles[0][1]
	# FileList = [y[2] for y in AllFiles][1:]
	for Direct in SubDirs:
		# File-by-file approach not used
		# try:
		# 	FileList = [x[2] for x in os.walk(Direct)][0]
		# except IndexError:
		# 	sys.stderr.write("{0} not a directory. Skipping...\n".format(Direct))
		# 	continue
		if Direct == '.':
			BaseDir = os.path.realpath('..')
			BaseName = os.path.split(BaseDir)[-1]
			SubDirName = os.path.realpath('.')
			DirectName =  os.path.split(SubDirName)[-1] 
		else:
			DirectName = Direct
			
		DirCount += 1
		sys.stderr.write("\n#---- Processing folder {0}\n".format(DirectName))
		

		eout = write_exif_to_folder(Direct,[BaseName, DirectName],CreditLine)
		if DEBUG:
			print eout[-1]
		if eout:
			NumberProcessed = int(eout[-1].strip().split(" ")[0])
			FileCount += NumberProcessed
			print NumberProcessed, "Exif Files"
		if eout and ("err" in eout):
			sys.stderr.write("** Error in EXIF: {0}\n{1}\n".format(Direct,eout))
			
		
		# folder-based xattr function -- not used
		xout = write_xattr_to_folder(Direct,[BaseName, DirectName])
		
		if DEBUG and xout:
			sys.stderr.write("** Output from xattr: {0}\n{1}\n".format(Direct,xout))

		# Either use glob.glob or os.walk or ...
				# The FileList has all files, including .DS_Store
		#print DirectName, "DIRNAME"
		# Trying by-file because 
		# This works for - but what about other options??
		
		FileList = glob.glob(DirectName + "/*") 
		# print FileList
		XFileSubCount = 0
		for FileName in FileList:
			fn = FileName.lower()
			ComboName = os.path.join(Direct,FileName)
			
			if (".jpg" in fn) or (".nef" in fn) or (".raw" in fn) or (".jpeg" in fn) or (".png" in fn) or (".tif" in fn):
				# sys.stderr.write("{0}, ".format(FileName.split(".")[:-1][0]))
				# writeexifkeywords(ComboName, [BaseName, Direct], CreditLine)
				# Changed from BaseName
				# sys.stderr.write("**Combo:{0}, File:{1}\n".format(ComboName,FileName))
				# writexattrs(FileName, [BaseName, Direct])
				dateline,datestr,freturn = getfilecreatedate(FileName)
				
				# Use exif date info to change dates.
				if dateline and (freturn < 1970):
				# writexattrs(F=Fname, DateTag = dateline)
					# print dateline, datestr, datestr[:12]
					#touchmoddate(F=Fname,DateTag = datestr[:12])
					setcreatedate(FileName,dateline)
					XFileSubCount += 1
					
				
			else:
				if len(fn) > 2:
					NonImageCount += 1	
					
		# if XFileSubCount != NumberProcessed:
		# 	sys.stderr.write("\n ** File count mismatch {0}[x] vs {1}[e] in folder {2}, ".format(XFileSubCount,NumberProcessed,Direct))
	# 	XFileCount += XFileSubCount
	# 	print "..."
	sys.stderr.write("\n ### Finished processing {0} exif and {2} changedate files in {1} folder(s). ({3} non-images)\n".format(FileCount,DirCount,XFileSubCount,NonImageCount))
	#sys.stderr.write("\n ### Finished processing {0} EXIF files in {1} folder(s)\n".format(FileCount,DirCount))



"""
NOTES: 
import xattr
F = "testfile.txt"
F = "DSC_0319.JPG"
xattr.setxattr(F,"_kMDItemUserTags",u"©Steven Haddock - MBARI")

xattr.listxattr('DSC_0319.JPG')

xattr.getxattr('DSC_0319.JPG','com.apple.FinderInfo')

import glob
flist = glob.glob("D*")
for F in flist:
	attrs = xattr.xattr(F)
	for name in attrs:
	   print "File:",F
	   print "------ Name:", name
	   print "     +---- X", repr(xattr.getxattr(F,name))
#	   print attrs.get(name)
 
 
 ## You do need com.apple.metadata: and also plist format.
kOMUserTags
kMDItemOMUserTags
_kMDItemUserTags
kMDItemkeywords
com.apple.metadata:kMDItemFinderComment

plistFront = '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><array>'
plistFront = '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><array>'
plistEnd = '</array></plist>'
plistTagString = plistTagString + '<string>{0}</string>'
xattr -w com.apple.metadata:_kMDItemUserTags "$plistFront$plistTagString$plistEnd" "$currentFile"

mdfind -onlyin ~ "(kOMUserTags == 'Tag1') && (kOMUserTags != 'Tag2')"
mdls -name kMDItemWhereFroms mydownload.gz

xattr -wx "com.apple.metadata:kMDItemFinderComment" "`xxd -ps CustomComment.plist`" NoComment

Check Caption-Abstract field...
exiftool -keywords=one -keywords=two -keywords=three DIR

"""
