#!/usr/bin/env python
#
# v2.5 Added -t --trim option to use shortened seq names (remove | and _18S from end)
# v2.41 Clean-up for consistency
# v2.4 added fasta mode to find fasta seqs in other dbs 2014-11-25
# v2.3 fixed error when list has empty lines 2014-10-16
# v2.2 added no-find option to ignore string searches 2014-03-27
# v2.1 clarified most exceptions to KeyError 2013/11/14
# v2.0 converted to argparse, to allow complex arguments 2013/08/17
# v1.6 reads in namelist only one time, added quiet mode 2013/08/16
# v1.5 will adapt if second file or list have redundant names 2013/07/24
# v1.4 allows use of multiple files 2013/04/04
# v1.3 corrected find list with ">" 26/03/2013
# v1.2 renamed to more conventional variables, also accept fasta names 20/03/2012
# v1.1 30/03/2012

"""getAinB.py - version 2.3 16/10/2014
Reads in some files in fasta format into a dictionary.
Then reads a list of names from a file and prints out the 
sequences in the list that match names (whole or part) in file A.
To print names which are not found, use -v (verbose)
To only print seqs and no stderr, use -q (quiet)

TODO:  Use parallel lists instead of dictionary?
TODO:  Invert search with -i
TODO:  Generate File not found errors
Usage:

getAinB.py namelist.txt FileA FileB > outputfile.fasta
getAinB.py namelist.txt FileA FileB -v > outputfile.fasta
getAinB.py searchstring FileA > outputfile.fasta
"""

import os
import sys
import argparse
from Bio import SeqIO
# implement this in next version 3.0
#from Bio import trie
#from Bio import triefind

def makeshort(n):
	if "|" in n:
		shortname = n.split("|")[0]
	elif n.count('_') > 1:
		shortname="_".join(n.split('_')[0:2])
	elif n.count(' ') > 1:
		shortname = "_".join(n.split()[0:2])
	else:
		shortname = n
	return shortname


def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	# this handles the system arguments, introduced in v2.0
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('query_name', help="file of names or search term")
	parser.add_argument('search_file', nargs="*", default = '-', help="fasta format files")
	parser.add_argument('-f','--fasta', action="store_true", help="assume query is normal fasta file")
	parser.add_argument('-q','--quiet', action="store_true", help="quiet, will not print stderr messages")
	parser.add_argument('-s','--no-search', action="store_true", help="ignore sequences that do not match exactly")
	parser.add_argument('-v','--verbose', action="store_true", help="verbose, print names which are not found")
	parser.add_argument('-t','--trim', action="store_true", default=False, help="trim name, cut gene name from end")
	args = parser.parse_args(argv)

	# query can be file name or a string
	query_name = args.query_name
	# subject must be a fasta format file
	# converted to list format v1.4
	file_list = [n for n in args.search_file if os.path.isfile(n)]

	# detect file, and read in lines one time (unlike before, which was reopened for each subject name in the file list)
	if os.path.isfile(query_name):
		usefile = True
		querylines = open(query_name, 'rU').readlines()
	else:
		usefile = False

	# keeps subject_name for each in file_list
	for subject_name in file_list:
		NotFound = 0
		Found = 0
		try:
			if not args.quiet:
				print >> sys.stderr, "# Reading sequences from %s" % (subject_name)
			seq_dictionary = SeqIO.to_dict(SeqIO.parse(open(subject_name,'rU'), "fasta"))
			# when done with the loop, print the sequences
			#for key in seq_dictionary.iterkeys():
				#print >> sys.stderr, key

			# in quiet mode, display no stderr
			if not args.quiet:
				print >> sys.stderr, "# Read %d sequences from %s" % (len(seq_dictionary),subject_name)

			# if argument is a file, then search for each line in the file
			if usefile:
				for line in querylines:
					searchstring = line.strip()
					# skip empty lines
					if not searchstring:
						continue
					# for fasta mode, if string is not a fasta id, then skip
					if args.fasta and not searchstring[0] == ">":
						continue
					# if names are directly from a fasta file (such as by grep) remove the ">"
					if searchstring[0] == ">":
						searchstring = searchstring[1:].strip()
						if args.trim:
							searchstring = makeshort(searchstring)
					try: # search the dictionary for precisely that term
						wayout.write("%s" % seq_dictionary[searchstring].format("fasta"))
						Found += 1
					except KeyError:
						try: # spaces in the fasta ID may mistake the exact name, so split is used
							splitline = searchstring.split(" ")[0]
							wayout.write("%s" % seq_dictionary[splitline].format("fasta"))
							Found += 1
						except KeyError:
							# to skip slow string searches, use the no-find flag
							if args.no_search:
								print >> sys.stderr, "## IGNORING", searchstring
							else:
								if args.verbose:
									print >> sys.stderr, "## STRING SEARCHING", searchstring
								try: # then try to find() the searchstring anywhere in the keys
									results = [key for key in seq_dictionary.iterkeys() if key.find(searchstring) != -1]
									if results:
									# if results exist then print them all, otherwise indicate flag as not found
										for result in results:
											wayout.write("%s" % seq_dictionary[result].format("fasta"))
											Found += 1
											# use the break to only take the first result and ignore the else
											#break
									else:
										NotFound+=1
										if args.verbose:
											print >> sys.stderr, "## NOT FOUND", searchstring
								except:
									NotFound += 1
									if args.verbose:
										print >> sys.stderr, "## NOT FOUND", searchstring
			# otherwise just search for some keyword
			else:
				searchstring = query_name
				try: # search the dictionary for precisely the searchstring
					wayout.write("%s" % seq_dictionary[searchstring].format("fasta"))
					Found += 1
				except KeyError:
					try: # otherwise try to find anywhere in the keys and print all findings
						results = [key for key in seq_dictionary.iterkeys() if key.find(searchstring) != -1]
						if results:
							for result in results:
								wayout.write("%s" % seq_dictionary[result].format("fasta"))
								Found += 1
						else:
							NotFound+=1
					except:
						NotFound += 1
		except ValueError: # for cases of duplicate names in dictionaries
			if not args.quiet:
				print >> sys.stderr, "# FASTA NAMES MAY BE REDUNDANT"
			seq_list = list(SeqIO.parse(open(subject_name,'rU'), "fasta"))

			if not args.quiet:
				print >> sys.stderr, "# Read %d sequences from %s" % (len(seq_list),subject_name)
			# if it is a file, read the file and search each term
			if usefile:
				for line in querylines:
					searchstring = line.rstrip()
					# if names are directly from a fasta file (such as by grep) remove the ">"
					if searchstring[0] == ">":
						searchstring = searchstring[1:]
					# search the dictionary for precisely that term
					results = [seq for seq in seq_list if seq.id.find(searchstring) != -1]
					if results:
						for result in results:
							wayout.write("%s" % result.format("fasta"))
							Found += 1
					else:
						# try instead to search for the description instead
						descres = [seq for seq in seq_list if seq.description.find(searchstring) != -1]
						if descres:
							for result in descres:
								wayout.write("%s" % result.format("fasta"))
								Found += 1
						else:
							NotFound+=1
							if args.verbose:
								print >> sys.stderr, "## NOT FOUND", searchstring
			else:
				searchstring = query_name
				results = [seq for seq in seq_list if seq.name.find(searchstring) != -1]
				if results:
					for result in results:
						wayout.write("%s" % result.format("fasta"))
						Found += 1
				else:
					NotFound+=1
		if not args.quiet:
			print >> sys.stderr, "## Found %d names from A in B"% (Found)
			print >> sys.stderr, "## Didn't find %d names" % (NotFound)
	# possibly indicate each file


if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)
