#!/usr/bin/env python3
'''Tile a set of images into a single Row x Column image.
Usage:
	image_tile.py -r 4 -c 6 --out TiledImage.png Frame*.jpg
	image_tile.py -r 3 -c 2 --gap 5 --color "#666666" --out TiledImage.jpg image*

If more images than grid size are specified, only the first images are used.
If fewer images than grid size are specified, it will leave blank spaces.

Options:
	--color to set the background color. Can be name or hex.
	--gap to set the padding between images.

Version 0.3: Allowed mismatch between image number and grid size
Version 0.2: Added argument parsing. 
Version 0.1: First working version. 
'''


from PIL import Image
import sys
import argparse

def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-r", "--rows",   type=int,  default = 3, help="How many rows in the final image")
	parser.add_argument("-c", "--cols",   type=int,  default = 4, help="How many columns in the final image")
	parser.add_argument("-g", "--gap",   type=int,  default = 2, help="Padding between images")
	parser.add_argument("--color",  default="white", help="Color for background. Can be name or hex")
	parser.add_argument("-o", "--out",  default="TiledImage.png", help="File name for tiled image")
	parser.add_argument("-b", "--debug",   action="store_true", dest="DEBUG", default=False, help="Debug output")
	parser.add_argument("Args", nargs=argparse.REMAINDER)
	options = parser.parse_args()
	return options

def main():
	Options = get_options()
	global DEBUG
	DEBUG = Options.DEBUG
	
	rows = Options.rows
	padcolor = Options.color
	cols = Options.cols
	gap = Options.gap
	outname = Options.out
	if Options.Args:
		imagelist = Options.Args[:]
	else:
		sys.exit("No image files specified.")
	if DEBUG:
		print("rows:", rows, "\ncols:", cols, "\ngap:", gap, "outname:\n", outname, "ImageFiles:", imagelist)

	if len(imagelist) < rows*cols:
		print("Fewer images than grid cells. Will leave {} blank spaces".format(rows*cols - len(imagelist)))
	if len(imagelist) > rows*cols:
		print("Warning: More images than grid size. Only using first", rows*cols, "images.")

	# ImagePattern = "/Users/haddock/temp/imagetest/tiletest/dark*.png"
	# imagelist = glob.glob(ImagePattern)

	imagelist.sort()
	if DEBUG:
		print ("processing",imagelist,file=sys.stderr)

	images = list(map(Image.open, imagelist))

	w = images[0].size[0] 
	h = images[0].size[1] 

	result = Image.new("RGB", (gap+(w+gap)*cols, gap+(h+gap)*rows), color=padcolor)
		
	print("Processing", len(imagelist), "images into a", rows, "x", cols, "grid with a", gap, "pixel gap.")

	for r in range(rows):
		for c in range(cols):
			i = r*(cols) + c
			try:
				result.paste(images[i], ((gap+(w+gap)*c, gap+(h+gap)*r)))
				if DEBUG:
					print (i, "+>", imagelist[i],file=sys.stderr)
			except IndexError:
				break
		
	result.save(outname)

if __name__ == '__main__':
	main()
