#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
#####################################################

  Pull frames from video. 
  Default is to pull frames at 10 second intervals.
  Use -n to pull a given number of frames instead.

#####################################################

Version 0.2 : Added timecode overlay and custom prefix
Version 0.1 : First working version

Requires cv2 
  -i  : Time interval between frames in seconds [Default: 10]
  -n  : Extract given number of frames instead of at interval
  -o  : Overlay image time onto image
  -c  : Specify color for overlay. Must be  "#hex". Default is "#EEEE00"
			If you specify a color, it will automatically turn on overlay
  -p  : Custom prefix for file names. Default is first 10 chars of Filename

Usage: 
	videograbframes.py -i 4 video.mp4
	videograbframes.py -n 5 video.mp4
	videograbframes.py -o -c "#FF0000" -p "VID" video.mp4
------------------------------------------------------------------------------
"""

import sys
import argparse
import time
import cv2


def get_options():
	'''TO DO Here, option for time interval or number of frames across whole video'''

	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-i", "--interval", type=int, default=10, help="Time between frames in seconds")
	parser.add_argument("-n", "--number",   type=int, default=0, help="Number of frames to extract")
	parser.add_argument("-b", "--debug",   action="store_true", dest="DEBUG", default=False, help="Debug output")
	parser.add_argument("-p", "--prefix", default="", help="Prefix for output files. Default is first 10 chars of Filename")
	parser.add_argument("-o","--overlay",  action="store_true", default=False, help='Overlay image time onto image')
	parser.add_argument("-c","--color",  default="#EEEE00", help='Color for image overlay. Must be  "#hex". Default is "#EEEE00"')


	parser.add_argument("Args", nargs=argparse.REMAINDER)
	options = parser.parse_args()
	return options


def hex_to_rgb(value):
	'''Convert hex color to RGB. Actually BGR for OpenCV'''
	value = value.lstrip('#')
	return tuple(int(value[i:i+2], 16) for i in (4, 2, 0))


def getduration_cv(filename):
	'''This is a bit faster than the FFmpeg version'''
	'''Get the duration of the video in seconds using CV2'''
	cap = cv2.VideoCapture(filename)
	fps = cap.get(cv2.CAP_PROP_FPS)
	frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
	duration = frame_count/fps
	return(duration)

def getframe_cv(cap,timecode):
	'''Get a frame from the video using CV2'''
	cap.set(cv2.CAP_PROP_POS_MSEC,timecode*1000)
	ret, frame = cap.read()
	return(frame)


if __name__ == "__main__": 
	Options = get_options()
	global DEBUG
	DEBUG = Options.DEBUG
	interval = Options.interval
	number = Options.number
	usecolor = hex_to_rgb(Options.color)

	if not Options.Args:
			sys.exit(__doc__)
	else: 

		FileList = Options.Args

		for File in FileList:
			if Options.prefix:
				Prefix = Options.prefix
			else:
				Prefix = File[:10]
			duration = getduration_cv(File)
			print(f"{File}: {duration}s")

			# Calculate the interval between frames instead of using time
			if number > 0:
				interval = int(duration/number)
				
			ints = range(1,int(duration),interval)
			print(f"Grabbing {len(ints)} frames at {interval} second intervals")

			starttime = time.time()
			cap = cv2.VideoCapture(File)
			for nexttime in ints:
				nextframe = getframe_cv(cap,nexttime)
				if Options.overlay or Options.color != "#EEEE00":
					image_width = nextframe.shape[1]
					image_height = nextframe.shape[0]
					cv2.putText(nextframe,f"{nexttime:04}",(int(image_width*.95),int(image_height-15)),cv2.FONT_HERSHEY_SIMPLEX,0.75,usecolor,2)
				cv2.imwrite(f"{Prefix}-frame_{nexttime:04}.png",nextframe)
			elapsed = time.time() - starttime
			timestring = f"Extracted {len(ints)} images in {elapsed:.2f} seconds"
			print(timestring)


