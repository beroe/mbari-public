###MBARI PUBLIC SOFTWARE REPO

>All software is provided with the provision that the user will assume risks of its operation. Bugs exist! Use with care and test with files that have been backed up.

File descriptions:

* **Argo** - files related to processing Argo float data
* **database** - database queries and post processing
* **fileutils** - general utilities of file and image manipulation
* **graphing** - `R` and other scripts for data processing
* **molecular** - genetic sequence analysis
* **oceanography** - calculation of oceanographic parameters

