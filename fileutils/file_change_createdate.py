#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
########
   Using EXIF creation data, update creation dates shown in Finder 
   Will only work on images...
   
   usage:
       {} *.JPG
   
   Requires OSX and possibly developer tools for the SetFile function
   
   version 1.1mb 
########
"""

import os
import sys
import subprocess

NoExifList = []

	
def	getfilecreatedate(filename):
	# use exiftool with epochsecond format
	exifcommand = 'exiftool -v0 -createdate -S  "%s" ' % (filename)
	# print exifcommand
	expipe = os.popen(exifcommand)
	exdata = expipe.readlines()
	# print "EXDATA,",exdata
	if exdata:
		# if you add -s it won't print this field to be stripped
		# exiftool  -s -FileAccessDate
		dateline = exdata[0].replace("CreateDate: ","")
		dates = dateline.replace(":","").replace(" ","")
		return dateline,dates
	else:
		dateline,dates = getvarsfilecreatedate(filename)
		return dateline,dates
		
def	getvarsfilecreatedate(filename):
	# use exiftool with epochsecond format
	exifcommand = 'exiftool -v0 -S -DateTimeCreated  "%s" ' % (filename)
	# print exifcommand
	expipe = os.popen(exifcommand)
	exdata = expipe.readlines()
	# print "EXDATA,",exdata
	if exdata:
		# if you add -s it won't print this field to be stripped
		# exiftool  -s -FileAccessDate
		dateline = exdata[0].replace("DateTimeCreated: ","")
		dateline = dateline.replace("+00:00","")
		dates = dateline.replace(":","").replace(" ","")
		return dateline,dates
	else:
		NoExifList.append(filename)
		return 0,0
	 
def setcreatedate(F,DateTag):
	DateList = DateTag.rstrip().split(" ")
	YearSwitch = DateList[0].split(":") # This has year first

	# Only do this if the CreateDate retrieved is recent...
	# Add to only do it if the CreatedDate of the file is messed up??
	if int(YearSwitch[0])> 1980:
		# Put the year last and join with /
		NewString = "/".join([ YearSwitch[1],YearSwitch[2],YearSwitch[0]])
		DateString = NewString + " " + DateList[1]
		# print DateString
		sys.stderr.write(".")
		setcommand = 'SetFile -d "{0}" "{1}"'.format(DateString,F)
		ProcString = subprocess.check_output(setcommand, stderr=subprocess.STDOUT,shell=True) 
	else:
		ProcString = ""
	return ProcString


if len(sys.argv)<2:
	sys.stderr.write(__doc__.format(sys.argv[0].split("/")[-1]))
else: 
	Flist = sys.argv[1:]
	offset = 1
	fcount=0
	for Fname in Flist:
		#print Fname
		# USE EXIF CREATE DATE 
		dateline,datestr = getfilecreatedate(Fname)
		if dateline:
		# writexattrs(F=Fname, DateTag = dateline)
			# print dateline, datestr, datestr[:12]
			#touchmoddate(F=Fname,DateTag = datestr[:12])
			setcreatedate(Fname,dateline)
			fcount+=1
		else:
			sys.stderr.write("X")
	sys.stderr.write("\nProcessed {} files.\n".format(fcount))
	if NoExifList:
		sys.stderr.write("Files with no exif: \n\t{}\n".format("\n\t".join(NoExifList)))
