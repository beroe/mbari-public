#plot a "heatmap" showing the size of a series of gene files
# uses RColorBrewer, but not required if you want to change UseColor = rainbow(16)

Path = "~/MBARI/Manuscripts/kogs/merged/"
PROTEIN = TRUE
# Expects a series of files called size_prot_something..., containing just a column of integers on their own lines, all in the same order (that order matching PTaxa below). 
# Derives the gene name/number through a regexp search for ...KOG1234... and grabs just the numbers from that.

# List of taxa in dataset, in the same order that the genes are in the individual files
PTaxa = c("AmphimedCEGpred","Amphimedon","Arabidopsis","Capitella","Celegans","Ciona","Daphnia","Drosophila","Human","Micromonas","Mleidyi","Mnemiopsis","Monosiga","OscarCEGpred","Oscarella","Salpingoeca","Scervisiae","Spombe","Urchin","abylopsis","aeginasp","apolemia","atolla","bathocyroe","bathyctena","belongata","benthiccteno","beroeforskali","calanoides","charistephane","chelophyes","chiroteuthis","cmacrocephala","cmultidentata","conchoecissa","cordagalma","craseoa","ctenoceros","desmophyes","diphyes","dosidicus","efowleri","erenna","eucalanus","euplokamis","flota","forskalia","frillagalma","gaussia","harmothoe","hippopodius","hormiphora","lampea","lilyopsis","lionsmane","lychnagalma","marrus","nannocalanus","nanomia","nepheloctenawhite","octopoteuthis","ocyropsis","phylliroe","pleuromamma","pterygioteuthis","rhizophysa","rosacea","sergestes","stephalia","tcamp1","tglobosa","thalassocalyce","tomopteris","vampyroteuthis","vermillionlobate","weirdcteno")

# paral
cats=c("POR","POR","PLA","POL","WOR","CHO","ART","ART","CHO","PRO","CTE","CTE","PRO","POR","POR","PRO","YEA","YEA","ECH","SIP","CNI","SIP","CNI","CTE","CTE","SIP","CTE","CTE","ART","CTE","SIP","MOL","WOR","SIP","ART","SIP","SIP","CTE","SIP","SIP","MOL","WOR","SIP","ART","CTE","POL","SIP","SIP","ART","POL","SIP","CTE","CTE","SIP","SIP","SIP","SIP","ART","SIP","CTE","MOL","CTE","MOL","ART","MOL","SIP","SIP","ART","SIP","PRO","PRO","CTE","POL","MOL","CTE","CTE")


if (PROTEIN) {
	MyList =  dir(Path,pattern = "^size_prot_.*")
	ThresholdSize = 10	
	Taxa = PTaxa
	UseTitle = "Protein lengths"
	}	else 	{
	MyList =  dir(Path,pattern = "^size_.*")
	ThresholdSize = 36
	Taxa = NTaxa
	UseTitle = "Nucleotide lengths"
}

print(paste("Files found:",length(MyList) ))


p = c()
allx = c()
allD = c()

for (FileName in MyList){
	KOGnum =sub(".*KOG([0-9]+).*","\\1",FileName)
	koglen = read.table(file=paste(Path,FileName,sep=""), header=FALSE, sep="\t")
	
	xvals = koglen$V1
	allx = rbind(allx,xvals)
#	HistMax = 1000
#	xvals[xvals>HistMax] = HistMax
	
	x = xvals

	# Blank out reads less than 10, so they are white squares in image...
	
	x[x<ThresholdSize]=NA
	
	# used to plot the difference from median instead of overall size...
	mx = median(x,na.rm=TRUE)
	LenDiff = x - mx
	allD = rbind(allD,LenDiff)
}

# could use NA here?
allx[allx<ThresholdSize] = 1

# convert false to zero or 1
p = (allx > ThresholdSize) * 1
   
# sort them in order by taxa and then by genes, from most to least

#Rows are taxa
RSum = rowSums(p)
logx = log(allx)
roworderp = p[rev(order(RSum)),] 
rowx = allx[rev(order(RSum)),]
rowd = allD[rev(order(RSum)),]

# columns are KOGs, 
CSum=colSums(roworderp)
colorderp = roworderp[,order(CSum)]

# These are the matrices of plotted values (raw and diff from median), in order
colx = rowx[,order(CSum)]
cold = rowd[,order(CSum)]


Get KOG names from file names
kognames = sub("\\..+$","",sub(".+_KOG","",MyList))

xd = data.frame(data=allx,row.names=kognames,check.names=FALSE)
colnames(xd)=Taxa

# library(gplots) # needed for rich.colors()

# not implemented UseColor = colorRampPalette(c("red", "orange", "blue"), space = "Lab")
# another option - UseColor = c("#FFFFFFFF",hcl(seq(0, 360, length = 20)))

# Added red at the end so a few outliers don't swamp color palette. 
# Could take log of colx instead
# UseColor = c(rainbow(16),"red","red","red","red","red","red","red")
# UseColor = c(heat.colors(16),"blue","blue","blue","blue","blue","blue")
# UseColor = c(rich.colors(16),"red","red","red","red","red","red","red","red")

# library(colorRamps)
# UseColor = c(blue2red(16),"red","red","red","red","red","red","red","red")
# UseColor = c(blue2green2red(32),"red","red","red","red","red","red","red","red")

library(RColorBrewer)

# display.brewer.all() # to see all palettes

# UseColor = c(rev(brewer.pal(32,"Purples")),"red","red","red","red","red","red","red","red")
# UseColor = c(brewer.pal(32,"Pastel1"),"red","red","red","red","red","red","red","red")
# UseColor = c(brewer.pal(32,"Pastel2"),"red","red","red","red","red","red","red","red")
# UseColor = c(brewer.pal(32,"Spectral"),"red","red","red","red","red","red","red","red")
# UseColor = c(brewer.pal(32,"Dark2"),"red","red","red","red","red","red","red","red")
UseColor = c(rev(brewer.pal(32,"Spectral"))
# UseColor = c(rev(brewer.pal(32,"Spectral")),"red","red","red","red","red","red","red","red")
# RdYlBu same as above
# UseColor = c(rev(brewer.pal(11,"RdYlBu")),"red","red","red","red","red","red","red","red")

# To plot in listed order instead of sorted order...
empty = '
 image((1:length(MyList)),(1:length(xvals)), axes=FALSE, xlab="", ylab = "", allx,mar=c(2,1,1,1),col=UseColor)
 title(paste(UseTitle," - unsorted",sep=""))
 axis(2,at=1:length(xvals),labels=Taxa,las=2, adj=1,cex.axis=0.6)
 axis(1,at=1:length(MyList),labels=kognames,las=2, adj=1,cex.axis=0.6)
'
colx[colx<6]=NA

### PLOT THE HEAT MAP 
image((1:length(MyList)),(1:length(xvals)), axes=FALSE, xlab="", ylab = "", colx,mar=c(2,1,1,1),col=UseColor)

#TO PLOT THE DIFF FROM MEDIAN INSTEAD, comment out the image() above and use this one...
# for median, Spectral is best...
# image((1:length(MyList)),(1:length(xvals)), axes=FALSE, xlab="", ylab = "", cold,mar=c(2,1,1,1),col=UseColor)



title(UseTitle)

# TODO: color by cats, might have to use text() instead of axis()
axis(2,at=1:length(xvals),labels=Taxa[order(CSum)],las=2, adj=1,cex.axis=0.6)
axis(4,at=1:length(xvals),labels=CSum[order(CSum)],las=2, adj=1,cex.axis=0.6)

axis(1,at=1:length(MyList),labels=kognames[rev(order(rowSums(p)))],las=2, adj=1,cex.axis=0.6)
axis(3,at=1:length(MyList),labels=RSum[rev(order(RSum))],las=2, adj=1,cex.axis=0.6)
