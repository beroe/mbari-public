#!/usr/bin/env python3

''' findblobs-image.py
Find blue or green blobs in an image and return the total area of the blobs

 Usage:
    findblobs-image.py -l -v -m 1000 -p 3 dark_bad_pixels_whiteblobs.jpg
	findblobs-image.py -h to see options

     -p [1-100] Percent of total image area covered by blogs to tag file [default=5]
	 -m [int]   Smallest blob size (in pixel area) to keep [default=1000]
	 -t [1-255] All values below this are set to back. Above this are considered blobs.

If you use -v to visualize, it should open image windows which you can close by pressing any key

NOTE: Due to a problem in the imread function you can't have spaces in the name of the image file

version 1.1.1 - minor reformatting - 2022-09-30
version 1.1   - lots of bugs and reformatting - 2022-09-29
version 1.0   - first working version - 2022-09-29
'''
import cv2
import numpy as np
import sys
import os

# trying without numpy
#kernel = np.ones((5,5),np.uint8)
#TODO: for other applications, can try Otsu's method for thresholding https://en.wikipedia.org/wiki/Otsu%27s_method


def get_options():
	import argparse
	parser = argparse.ArgumentParser(usage = __doc__) 
	parser.add_argument("-b", "--debug", action="store_true", default=False, help="debug")
	parser.add_argument("-t", "--threshold", action="store",type=float, default=0.0, help="Manually define the minimum threshold intensity [0-255]")
	parser.add_argument("-m", "--minimum", action="store", type=int, default=1000, help="Smallest blob size to keep [default=1000]")
	# parser.add_argument("-a", "--area", action="store", type=int, default=1000, help="Smallest total area to tag file [default=1000]")
	parser.add_argument("-p", "--percent", action="store", type=float, default=2.0, help="Percent of total area to tag file [default=5]")
	parser.add_argument("-v", "--visualize", action="store_true", default=False, help="Show plots of results")
	parser.add_argument("-s", "--save", action="store_true", default=False, help="Save Annotated Image")
	parser.add_argument("-r", "--keepred", action="store_true", default=False, help="Keep the red layer of the image")
	parser.add_argument("-l", "--labelfile", action="store_true", default=False, help="Write color code to xattr")
	parser.add_argument("Args", nargs='*')
	options = parser.parse_args()
	return options

################################################################################################

def writexattrs(F,TagList):
	""" writexattrs(F,TagList):
	writes the list of tags to three xattr field on a file-by file basis:
	"kMDItemFinderComment","_kMDItemUserTags","kMDItemOMUserTags
"""
	import xattr
	from sys import platform as _platform
	if _platform == "darwin":
		try:
			import xattr
		except ModuleNotFoundError:
			print("xattr module not found - install with pip3 install xattr")
			return

		plistFront = '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd"><plist version="1.0"><array>'
		plistEnd = '</array></plist>'
		plistTagString = ''
		for Tag in TagList:
			plistTagString = plistTagString + '<string>{}</string>'.format(Tag.replace("'","-"))
		TagText = plistFront + plistTagString + plistEnd

		# CommentText = "Trying again"
		# Comment = "bplist00_" + CommentText
		# xattr.setxattr(F,OptionalTag+"kMDItemFinderComment",Comment.encode('utf8'))

		OptionalTag = "com.apple.metadata:"
		XattrList = ["_kMDItemUserTags"]
		# Optional extended list of tags
		# XattrList = ["kOMUserTags","kMDItemOMUserTags","_kMDItemUserTags","kMDItemkeywords"]
		for Field in XattrList:
			# using module instead of subprocess....
			xattr.setxattr(F,OptionalTag+Field,TagText.encode('utf8'))


################################################################################################

def findblobs(Fn,MinThresh,minblob,percent):

	CONNECTIVITY = 4
	DRAW_CIRCLE_RADIUS = 24
	safepath = Fn.replace(" ", "\ ")


	img = cv2.imread(safepath)
	if img is None:
		print("# Error reading file. Possible space in file name: {}".format(Fn))
		return 0,0,0,0

	width = img.shape[1]
	height = img.shape[0]
	totalarea = 0

	imagearea = width*height

	zerolist = np.zeros([height,width], np.uint8)

	# this will get overwritten if keepred is set
	# nored = img.copy()
	minuswhite = img.copy()

	if not(OPTIONS.keepred):
		# this version just drops the red channel, instead of red and white
		# nored[:,:,2] = np.zeros([img.shape[0], img.shape[1]])


		# This section removes the red and white blobs
		blue, green, red = cv2.split(img)
		newblue  = blue.astype(np.int16)-red.astype(np.int16)
		newgreen = green.astype(np.int16)-red.astype(np.int16)
		#if value is negative, set to zero
		newblue[newblue < 0] = 0
		newgreen[newgreen < 0] = 0
		minuswhite = cv2.merge((newblue.astype(np.uint8),newgreen.astype(np.uint8),zerolist))

	# img.shape returns a tuple of number of rows, columns, and channels (if image is color)

	# could use nored here instead

	grayimg = cv2.cvtColor(minuswhite, cv2.COLOR_BGR2GRAY)

	y1=25
	y2=75

	# Two boxes in the corners.
	rois = [grayimg[y1:y2,y1:y2],grayimg[height-y2:height-y1,width-y2:width-y1]]

	#draw rectangles around the boxes
	cv2.rectangle(img,(y1,y1),(y2,y2),(0,255,0),1)
	cv2.rectangle(img,(width-y2,height-y2),(width-y1,height-y1),(0,255,0),1)

	if not (MinThresh):
		MinThresh = np.mean(rois) +1

	# minthresh = 20 # in the program set this to the average of the (median filtered?) image
	maxthresh = 255
	#TODO: TEST WITH cv2.THRESH_BINARY+cv2.THRESH_OTSU
	ret, thresh = cv2.threshold(grayimg, MinThresh, maxthresh, cv2.THRESH_BINARY)
	#erosion = cv2.erode(thresh,kernel,iterations = 1)
	#erosion = cv2.erode(thresh,(12,12),iterations = 1)
	medfilt = cv2.medianBlur(thresh, 5)  # THIS WORKS WELL

	components = cv2.connectedComponentsWithStats(medfilt, CONNECTIVITY, cv2.CV_32S)

	(numLabels, labels, stats, centers) = components

	areas = stats[1:,4]
	totalarea = np.sum(areas)
	NumBlobs = len(areas)
	Minpix = np.min(grayimg)
	Maxpix = np.max(grayimg)
	basefile = os.path.basename(Fn)
	# the zeroeth entry is the whole image, so we want to start at 1
	if totalarea == imagearea:
		print("{}\tNA\tNo blobs found {}".format(basefile),file=sys.stderr)
		return 0,0,0,0

	if OPTIONS.visualize or OPTIONS.save:
		for center,area in zip(centers[1:],areas):
			if area > minblob:
				cv2.circle(img, (int(center[0]),int(center[1])), DRAW_CIRCLE_RADIUS, (0,0,255), 1)
				cx=int(center[0])
				cy=int(center[1])
				# what is the order of the colors? BGR, I think.
				cv2.circle(img, (cx,cy), DRAW_CIRCLE_RADIUS, (0,240,240), thickness=1)
				# cv2.putText(img, (str((int(area)))), (cx-10,cy),1,1.5,(0,255,255),2)
				cv2.putText(medfilt, (str((int(area)))), (cx-40,cy),1,1.5,(128,128,128),2)
		# left labels
		cv2.putText(img, ("BlobPct: " + "{:.1f}/{:.1f}%".format(100*totalarea/imagearea,percent)), (y1,y1-2),1,1,(0,240,240),2)
		cv2.putText(img, ("Blobs: " + str((int(NumBlobs)))), (y1,y2+16),1,1,(0,240,0),2)
		cv2.putText(img, ("Thresh: " + str((int(MinThresh)))), (y1,y2+32),1,1,(0,240,0),2)
		cv2.putText(img, ("MinBlob: " + str((int(minblob)))), (y1,y2+48),1,1,(0,240,0),2)
		# right labels
		cv2.putText(img, ("ImageArea: "), (width-120-y1,y1-2),1,1,(0,240,240),2)
		cv2.putText(img, (str((int(imagearea)))), (width-120-y1,y1+14),1,1,(0,240,240),2)
		cv2.putText(img, ("PixRange: {}:{}".format(Minpix,Maxpix)), (width-140-y1,y1+30),1,1,(240,240),2)
		
		if OPTIONS.save:
			cv2.imwrite('overlay-'+basefile,img)
			print("Saved overlay-{}".format(basefile),file=sys.stderr)
		if OPTIONS.visualize:	
			cv2.imshow('MinusWhite', minuswhite)
			cv2.imshow('Medfilt', medfilt)
			cv2.imshow('Original', img)
			# cv2.imshow('Thresh', thresh)
			cv2.waitKey(0)
			cv2.destroyAllWindows()

	TotalBlobPct = 100*totalarea/imagearea
	if OPTIONS.debug:
		print("ImageArea: " + str((int(imagearea))),file=sys.stderr)
		print("BlobPct: " + "{:.1f}% / {}%".format(TotalBlobPct,percent),file=sys.stderr)
		print("Thresh: " + str((int(MinThresh))),file=sys.stderr)
		print("BlobArea: " + str((int(totalarea))),file=sys.stderr)
		print("NumBlobs: " + str((int(NumBlobs))),file=sys.stderr)
		print("PixRange: {}:{}".format(Minpix,Maxpix),file=sys.stderr)

	return NumBlobs,totalarea,TotalBlobPct,imagearea

#TODO: FIX percent or go back to basic area. It is mixed up now between the sum of 
# the blob areas vs the area of an individual blob.

def main():
	Opt = get_options()
	NumBlobs,TotalArea,TotalBlobPercent,ImageArea = 0,0,0,0
	global OPTIONS
	OPTIONS = Opt
	if OPTIONS.debug:
		print(OPTIONS,file=sys.stderr)
	# test file name — ideally overwritten?
	FileName = '/Users/haddock/temp/imagetest/dark_bad_pixels_whiteblobs.jpg'
	if OPTIONS.Args:
		FileName = OPTIONS.Args[0]
		# print >> sys.stderr, "Using FILE", FileName	
	else:
		print("No file name given, using test file",file=sys.stderr)

	NumBlobs,TotalArea,TotalBlobPercent,ImageArea = findblobs(FileName,OPTIONS.threshold,OPTIONS.minimum,OPTIONS.percent)
	MinTotal = OPTIONS.percent * ImageArea/100
	if TotalArea and (TotalArea > MinTotal):
		if OPTIONS.labelfile:
			LabelColor=["Yellow"]
			if TotalArea > MinTotal * 2:
				LabelColor=["Red"]
			writexattrs(FileName,LabelColor)
			print("Wrote tag {} to file".format(LabelColor[0]), file=sys.stderr)
			if OPTIONS.save:
				writexattrs("overlay-"+os.path.basename(FileName),LabelColor)

		# TODO: Print the percent totalarea instead of pixel count
		print(os.path.basename(FileName)+"\t{}\t{:.1f}%".format(NumBlobs,TotalBlobPercent))
	else:
		print(os.path.basename(FileName)+"\tNA\tZero found",file=sys.stderr)

if __name__ == '__main__':

	main()


######################################################################################################################################################

'''
NOTE: ??Try dilate before .erode
'''