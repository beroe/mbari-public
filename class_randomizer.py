#! /usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import readchar
from random import shuffle, choice
import time
import sys

def phrases():
	PhraseList = ["Great job","Super","Fantastic",
	"Awesome","Congrats", "Love it","Not bad","All righty then",
	"He shoots, he scores","Nailed it",	
	"That is what I am talking about","Incredible","Excellent"]
	return PhraseList
	
def makeshuffle():
	ClassString = """Arturo
		Jimmy
		Bill
		Anela
		Zander
		Michelle
		Jason
		Jasmine
		Liz
		Emily
		Linda
		Sara
"""

	ClassList = ClassString.split()
	# print ClassList
	shuffle(ClassList)
	shuffle(ClassList)
	return ClassList

def figlet(Person):
	Command = 'figlet "   *  {}  * "'.format(Person)
	ProcOut = subprocess.Popen(Command,stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True)
	Output = ProcOut.stdout.read()
	OutList = Output.split('\n')
	InList = [" "*8 + L for L in OutList ]
	ReturnString = "\n".join(InList)
	return ReturnString
	
def sayname(Person):
	TranslateDict = {"Michelle": "Me shell", "Jason":"Venomix man"}
	Phonetic = TranslateDict.get(Person,Person)
	Command = 'say ' + Phonetic
	ProcOut = subprocess.check_call(Command,stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True)
	time.sleep(.5)

def spincursor():
	import itertools, sys
	spinner = itertools.cycle(['-', '/', '|', '\\'])
	while True:
	    sys.stdout.write(spinner.next())  # write the next character
	    sys.stdout.flush()                # flush stdout buffer (actual character display)
	    sys.stdout.write('\b')            # erase the last written char



def newspin(loops=10):
	syms = ['\\', '|', '/', '-']
	bs = '\b'

	print  " "*15  +  ".... S H U F F L I N G   C L A S S   L I S T ...\\",
	for _ in range(loops):
		for sym in syms:
			sys.stdout.write("\b%s" % sym)
			sys.stdout.flush()
			time.sleep(.2)
		
def main():
	Phrase = phrases()
	Ordered = makeshuffle()
	print Ordered
	print "\n"*50
	newspin(5)
	print "\n"*50
	print "\n\n"
	print "                        ..... R E A D Y ......"
	for j in range(5): print "\n"; time.sleep(.4)
	# print "\n"*8
	ind = 0
	kch = 'q'
	while ind < len(Ordered):
		Person = Ordered[ind]
		kch = readchar.readchar()
		print "> "
		if kch == 'x':
			break
		print "\n"*9
		print figlet(Person)
		print " \n                #{} of {}".format(ind,len(Ordered)-1) 
		print"\n"*6
		sayname(Person)
		ind +=1
		kch = readchar.readchar()
		if kch == 'x':
			break
		print "\n"*12
		print " \n         >  {}  <".format(Phrase[0]) 
		print"\n"*6
		
		sayname(Phrase[0])
		for j in range(10): print "\n"; time.sleep(.3)
		Phrase = Phrase[1:] + [Phrase[0]]



		
if __name__ == '__main__':
	# newspin()
	# print "Prompt"
	main()
