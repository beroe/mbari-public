#! /usr/bin/env python

""" This program downloads a table from a web page 
and extracts values from lines within the table using 
regular expressions searches. (Regular expressions are 
used instead of XML tag parsing because they are more 
universal for other types of input files.) 

Although this program is set-up to load a web site, it 
would work equally well with a file as input. Instead of 
counting lines to skip non-data values, it uses the regexp 
match to sort out what lines to retain."""

import urllib
import re
import sys

# Open a URL: This could also work from a local file using:
# FileName = "data.html"
# Content = file(MyFileName,'rU')
# In that case, the rest (reading lines from Content) would be just the same 

# There are lots of URL-related libraries, including Beautiful Soup
# This is the simplest case of just grabbing content
MyUrl = "http://practicalcomputing.org/aminoacid.html"
Content = urllib.urlopen(MyUrl)

# View the source of that page above in a browser to see its structure
# Below is an example of the line format we want to parse. The first field
# may contain spaces, and the third can have punctuation
# <tr><td>Valine</td><td>Val</td><td>V</td><td>117.15</td></tr>


# Use an 'r' before the strings for regular expressions so that the 
# backslashes are not interpreted by python, but sent to the 
# re search unaltered...
# Captured fields (surrounded by parentheses) will populate the 
# .group() and .groups() values

SearchStr = r"<tr><td>[\w\ ]+</td><td>\w+</td><td>(.)</td><td>([\d\.]+)</td></tr>"

# Set up a file name for writing. Optional -- you could capture
# text using redirection >
OutName = "testout.txt"

# do some optional steps if they want to write out to a file
# Turn this on/off to enable file creation
WriteOut = False
if WriteOut:
	# Careful with the name! Opening a file will erase it
	# The corresponding close tag is below
	OutStream = file(OutName,'w')

# initialize some counters
LineNum = 0
DataCount = 0

# the name Lines here is arbitrary -- doesn't have special power to get lines
# Read each line from the web source or the Content
# Remember that HTML doesn't need to be separated by lines, so this might
# not work on a perfectly readable web page.

for Lines in Content:
	# syntax for searching: (SearchTerm, StringToSearch)
	# The result is a special re object, not a list or anything until
	# you extract the values
	Result = re.search(SearchStr,Lines)
	LineNum += 1   # Count all lines in the file/web source
	
	# The result
	if Result:
#		print Result.groups() # for testing -- just see what is matched
		DataCount +=1
		print "%s\t%s" % (Result.group(1), Result.group(2))
		if WriteOut:
			OutStream.write("%d\t%s\t%s\n" % (LineNum,Result.group(1), Result.group(2)))
			
	# No match, don't freak out. Just skip non matching lines and report via stderr
	else:
		sys.stderr.write( "# No Match: %s\n" % (Lines.rstrip()) )
		
sys.stderr.write ("### Found %d data lines out of %d total lines.\n" % (DataCount,LineNum) )

if WriteOut:
	OutStream.close()