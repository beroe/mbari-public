#! /usr/bin/env python
# 
# This could easily be modified to run with other blast programs, or to use 
# a different program depending on a run-time parameter.
#
# ver. 5.3 - fixed bug for blastn, and 'ac' regexp in blastp 25/oct/2012
# ver. 5.2 - updated for blast 2.2.26+ 07/aug/2012
# ver. 5.1 - added blast-translate mode 20/jun/2012
# ver. 5.0 - changed to argparse input, rather than sys 11/jun/2012
# ver. 4.7 - added mouse database with -u, columns for query length and identity 01/jun/2012
# ver. 4.6 - added column for frame 28/mar/2012
# ver. 4.5 - added column for number of alignments 2/mar/2012
# ver. 4.4 - corrected bug with absolute file paths 23/jan/2012
# ver. 4.3 - returns hitsfile as well, added option for no-header line 17/jan/2012
# ver. 4.2 - added blastp and blastn 10/jan/2012
# ver. 4.1 - fixed output error with dictionary, reorganized 4/jan/2012
# ver. 4   - reworked to blast in blocks of 1000 and also parse output
#            files as findAinB after blasting, for speed reasons 3/jan/2012
# ver. 3.5 - configured output for nr and trembl, added return of nomatches 
#            to integrate with pipelines 31/dec/2011
# ver. 3.4 - reconfigured for trembl on server
# ver. 3.3 - fixed file output error, formats 21/dec/2011
# ver. 3.2 - utilizes cStringIO to run stdout as input for ncbixml.read()
#            also outputs fasta file of matches 
# ver. 3.1 - cleaned up output error, added nr option
# ver. 3.01- integrates fasta parser to both allow progress report and
#            output of non-matches to fasta format 14/dec/2011
# ver. 2.4 - adds os to write non-matches to another file 12/dec/2011
# ver. 2.3 - corrected bug with accession number for swissprot 1/dec/2011
# ver. 2.2 - Accounted for non-matching sequences
# ver. 2.1 - Fixed ?error about /tmp folder. Added flag to search trembl database
# ver. 2   - rewritten to work with blastplus (blast+)
# ver. 1   - 12 Jun 2009

usage="""
blastplustable.py

By default:
Run blastx against database (swissprot) and print out a table of results
table fields separated by tabs then writes out sequences with no match.

Output consists of:
"Resulttitle", "Evalue", "Align_length", "Query", "Accession", "Matched"
"Num_Align", "Frame", "Subject Length", "Identities", "Query Length"

Requires biopython (1.55+) and blast+ package installed on your system

This could take a long time to finish.

Usage: 
  blastplustable.py -q QUERYFILE -x s

  blastplustable.py -q sequencelist.fta -x s > results_table.txt

  # for other databases, use -x t, r, n, c, m, a, u
  blastplustable.py -q sequencelist.fta -x t > results_table.txt

  # for other programs, use -b PROGRAM
  blastplustable.py -q sequencelist.faa -x s -b blastp > results_table.txt

-x t trembl database
-x r nr (non-redundant) protein database
-x n nt (uses blastn)
-x c for capitella filtered protein set
-x m for mnemiopsis reference ests (use blastn, tblastn or tblastx)
-x a for nematostella (anemone) protein database
-x u for mus musculus (mouse) protein database
"""

import sys
import argparse

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=usage)
	parser.add_argument('-q','--query', help="query file")
	parser.add_argument('-d','--db', help="database file - default: swissprot", default="uniprot_sprot.fasta")
	parser.add_argument('-b','--blast-program', help="blast program - default: blastx", default="blastx")
	parser.add_argument('-o','--output-tag', help="label to append to files - default: sp", default="sp")
	parser.add_argument('-s','--search-tag', help="label to index blast hits - default: sp", default="sp")
	parser.add_argument('-S','--use-search', action="store_true", help="use search string, instead of accession number")
	parser.add_argument('-H','--no-header', action="store_false", help="omit header line in output table")
	parser.add_argument('-M','--no-match-file', action="store_false", help="do not write hits and nomatch output files")
	parser.add_argument('-t','--translate', action="store_true", help="based on the alignment, print a translation (use with blastx)")
	parser.add_argument('-p','--processors', help="number of processors", default='16')
	parser.add_argument('-e','--evalue', type=float, help="evalue cutoff blast search - default: 0.00001", default=0.00001)
	parser.add_argument('-m','--max-seqs', type=int, help="maximum number of records per blast search - default: 2", default=2)
	parser.add_argument('-x','--defaults', help="for specific database defaults - this will override other parameters; use -x t, etc")
	args = parser.parse_args(argv)

	import time
	import re
	import os.path

	#if os.path.isfile(args.query):
	blast_file = os.path.abspath(args.query)

	# establish default blasting parameters for single commandline run
	ProgramName = args.blast_program
	blast_db = args.db
	# outputtag is used for automated naming of hits/no-hits
	outputtag = args.output_tag
	# searchheader is used to pull accession numbers from the description
	searchheader = args.search_tag
	MaxNum = args.max_seqs

	# set alternate parameters for commandline arguments
	if (args.defaults=='s'):
		blast_db   = "uniprot_sprot.fasta"
		outputtag = "sp"
		searchheader = outputtag
		args.use_search = True
	elif (args.defaults=='t'):
		blast_db   = "uniprot_trembl.fasta"
		outputtag = "tr"
		searchheader = outputtag
		args.use_search = True
	elif (args.defaults=='r'):
		blast_db   = "nr"
		outputtag = "nr"
		searchheader = outputtag
	elif (args.defaults=='n'):
		blast_db   = "nt"
		ProgramName = "blastn"
		outputtag = "nt"
		searchheader = outputtag
	elif (args.defaults=='c'):
		blast_db   = "Capitella_FilteredModelsv1.0.aa.fasta"
		outputtag = "ca"
		searchheader = "Capca1"
		args.use_search = True
	elif (args.defaults=='m'):
		blast_db   = "mnemiopsis_est_lib.fasta"
		outputtag = "mn"
		searchheader = "gb"
		args.use_search = True
	elif (args.defaults=='a'):
		blast_db   = "nematostella_prot_lib.fasta"
		outputtag = "an"
		searchheader = "gi"
		args.use_search = True
	elif (args.defaults=='u'):
		blast_db   = "mouse-uniprot-reviewed.fasta"
		outputtag = "mo"
		searchheader = "sp"
		args.use_search = True

	import cStringIO
	from Bio import SeqIO
	from Bio.Blast.Applications import NcbiblastxCommandline
	from Bio.Blast import NCBIXML

	# the blast command
	## must change num_threads if running on a different system
	Blastx_Command = NcbiblastxCommandline(cmd= ProgramName, query="-", db=blast_db, evalue=args.evalue, outfmt=5, num_threads=args.processors, max_target_seqs = MaxNum)#, num_alignments=MaxNum, word_size=4)
	# num_descriptions and num_alignments removed for blast-2.2.26+

	searchstring = searchheader + "\|([\w\.]+)\|"
	if args.no_match_file:
		hitsoutputfilename = os.path.splitext(os.path.abspath(blast_file))[0] + "_" + outputtag + "_hits.fasta"
		hitsoutfile = open(hitsoutputfilename, 'w')
		nomatchoutputfilename = os.path.splitext(os.path.abspath(blast_file))[0] + "_" + outputtag + "_nom.fasta"
		nomatchoutfile = open(nomatchoutputfilename, 'w')
	if args.translate:
		transoutputfilename = os.path.splitext(os.path.abspath(blast_file))[0] + "_" + outputtag + "_prots.fasta"
		transoutfile = open(transoutputfilename, 'w')
		from Bio.Seq import Seq
		from Bio.Alphabet import IUPAC
		from Bio.SeqRecord import SeqRecord

	## set to true to print raw info about each record
	debug = False

	## start main
	recnum = 0
	nomatch = 0
	blocksize = 1000

	# imports the sequences twice, as list and as dictionary
	# the list gets chopped while the dictionary is used to find the hit/no-hit sequences
	print >> sys.stderr, '# Reading sequences: ', time.asctime()
	seq_dictionary = SeqIO.to_dict(SeqIO.parse(blast_file, "fasta"))
	seq_records = list(SeqIO.parse(blast_file, "fasta"))
	seqreclength = float(len(seq_records))
	print >> sys.stderr, '# Finished reading',str(len(seq_records)),'sequences: ', time.asctime()
	startclock=time.asctime()
	starttime= time.time()
	print >> sys.stderr, '# Started BLAST on file',blast_file,'at: ', startclock
	print >> sys.stderr, 'COMMAND:', Blastx_Command

	# print the header line for the table
	if args.no_header:
		print >> wayout, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % ("Resulttitle", "Evalue", "Align_length", "Query", "Accession", "Matched", "Num_Align", "Frame", "Subject Length", "Identities", "Query Length") 


	### idea: possibly implement as enumerate(seq_records)
	# generates input as a list of n sequences, where n = blocksize
	# then removes those from the sequence list until there are none left
	for setnum in range(len(seq_records)//blocksize+1):
		subsequences = ""
		if (len(seq_records)>=blocksize):
			recnum+=blocksize
			for i in range(blocksize):
				subsequences += seq_records[i].format("fasta")
			seq_records = seq_records[blocksize:]
		else:
			recnum+=len(seq_records)
			for i in range(len(seq_records)):
				subsequences += seq_records[i].format("fasta")
			seq_records = []

#		result_handle = os.popen("echo \""+seq_record.format("fasta")+"\" | "+ str(Blastx_Command))

#		result_handle = Blastx_Command(stdin=seq_record.format("fasta"))
#		resultout = cStringIO.StringIO(result_handle[0])

		result_handle = cStringIO.StringIO(Blastx_Command(stdin=subsequences)[0])

#		result_handle = cStringIO.StringIO(Blastx_Command(stdin=seq_record.format("fasta"))[0])
#		result_handle = open("/tmp/blastout.xml")
		sys.stderr.write(str(recnum)+" records: %.1f" % (recnum/seqreclength*100) + "%: " +str(time.asctime())+"\n")

		if result_handle:
			for record in NCBIXML.parse(result_handle):
				# record = NCBIXML.parse(result_handle)
				# originally used try because if there is no match, just skip the record
				# there was minimal change in time for using len, but was better for debugging
				if len(record.alignments):
					rt = record.alignments[0].hit_def[:80]
					ev = record.descriptions[0].e
					al = record.alignments[0].hsps[0].align_length
					rq = record.query
					if args.use_search:
						try:
							ac = re.search(searchstring,record.descriptions[0].title).group(1)
						# for cases where NoneType is flagged, possibly due to database errors
						except AttributeError:
							ac = record.alignments[0].accession
					else:
						ac = record.alignments[0].accession
					#if blast_db == 'nr' or blast_db == 'nt':
					#	ac = record.alignments[0].accession
					if ProgramName == "blastn":
						ms = record.alignments[0].hsps[0].query.replace(' ','.')
					else:
						ms = record.alignments[0].hsps[0].match.replace(' ','.')
					na = len(record.alignments)
					qf = record.alignments[0].hsps[0].frame[0]
					sl = len(record.alignments[0].hsps[0].sbjct)
					ql = record.query_length
					qi = record.alignments[0].hsps[0].identities
					ip = qi/float(ql)
					# THE REAL ONE LINER
					# any error in this line will make this record a NON MATCH
					print >> wayout, "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" % (rt, ev, al, rq, ac, ms, na, qf, sl, qi, ql)
					# hitacs.append(record.query)
					if args.no_match_file:
						try:
							hitsoutfile.write("%s" % seq_dictionary[record.query].format("fasta"))
						except:
							hitsoutfile.write("%s" % seq_dictionary[record.query.split(" ")[0]].format("fasta"))
					if args.translate:
						try:
							untransseq = seq_dictionary[record.query]
						except:
							untransseq = seq_dictionary[record.query.split(" ")[0]]
						align_frags = ms.replace('+','.').split('.')
						align_lens = [len(afrag) for afrag in align_frags]
						align_piece = align_frags[align_lens.index(max(align_lens))]
						if qf < 0:
							transseq = untransseq.seq.reverse_complement()[(abs(qf)-1):].translate()
						else:
							transseq = untransseq.seq[((qf)-1):].translate()
						preorfs = transseq.split('*')
						orfs = [orf for orf in preorfs if align_piece in orf]
						metorfs = [orfs[i][orfs[i].find('M'):] for i,morf in enumerate(orfs) if morf.count('M')]
						if metorfs:
							fragmentlengths = [len(fragment) for fragment in metorfs]
							longestfragment = metorfs[fragmentlengths.index(max(fragmentlengths))]
						else:
							fragmentlengths = [len(fragment) for fragment in orfs]
							longestfragment = orfs[fragmentlengths.index(max(fragmentlengths))]
						untransseq.seq = longestfragment
						transoutfile.write("%s" % untransseq.format("fasta"))
					# This is all only printed out if you are in "debug" mode
					if debug:
						print >> sys.stderr, "##### ",record.query
						print >> sys.stderr, "## q letters ",record.query_letters
						print >> sys.stderr, "## q id ",record.query_id
						print >> sys.stderr, "# DESCR:", d
						# print >> sys.stderr, "<RECORD"
						print >> sys.stderr, "http://www.uniprot.org/uniprot/"+d.accession+".fasta"
						print >> sys.stderr, "drecord ", re.search("sp\|(\w+)\|",shorttitle).group(1)
						print >> sys.stderr, "expect:",d.e
						print >> sys.stderr, "## ShortTitle: ", shorttitle
						print >> sys.stderr, "len(rec.aln):",len(record.alignments)
						print >> sys.stderr, "al.len: ", al.length, al.hsps[0].align_length
						print >> sys.stderr, "al.hsp.matchstr: ", al.hsps[0].match.replace(' ','-')
					# for al in record.alignments[:3]:
					# 	# for hsp in alignment.hsps[0]:
					# 	hsp = al.hsps[0]
					# 	# print '****Alignment****'
					# 	print "###########",  record.query
					# 	print al.title[:50], al.length, 'bp', len(al.hsps), 'HSPs' 
					# 	print al.title, al.length, hsp.expect
				else:
					# no match output
					# sys.stderr.write("###  No match for %s\n" % record.query)
					nomatch+=1
					# nomatchacs.append(record.query)
					if args.no_match_file:
						try:
							nomatchoutfile.write("%s" % seq_dictionary[record.query].format("fasta"))
						except:
							nomatchoutfile.write("%s" % seq_dictionary[record.query.split(" ")[0]].format("fasta"))
			result_handle.close()
		else:
			print >> sys.stderr, "ERROR - no handle"
	print >> sys.stderr, '# Started BLAST: ', startclock
	print >> sys.stderr, '# Finished BLAST: ', time.asctime()
	print >> sys.stderr, "# Processed %d records in %.1f minutes" % (recnum, (time.time()-starttime)/60)
	if args.no_match_file:
		print >> sys.stderr, "# %d records had hits, written to %s" % ((recnum-nomatch),hitsoutputfilename)
		print >> sys.stderr, "# %d records had no match, written to %s" % (nomatch, nomatchoutputfilename)
		hitsoutfile.close()
		nomatchoutfile.close()
		# keep this to integrate into pipelines
		# the nomatch output from one search can be used as input for the next search
		return hitsoutputfilename, nomatchoutputfilename
	if args.translate:
		transoutfile.close()
	else:
		print >> sys.stderr, "# %d records had hits" % ((recnum-nomatch))
		print >> sys.stderr, "# %d records had no match" % (nomatch)
		return recnum	

"""
	###########################################################
	# contents of a record in records:

['__doc__', '__init__', '__module__', 'alignments', 'application', 'blast_cutoff', 
	'database', 'database_length', 'database_letters', 'database_name', 
	'database_sequences', 'date', 'descriptions', 'dropoff_1st_pass', 
	'effective_database_length', 'effective_hsp_length', 'effective_query_length', 
	'effective_search_space', 'effective_search_space_used', 'expect', 'filter', 
	'frameshift', 'gap_penalties', 'gap_trigger', 'gap_x_dropoff', 'gap_x_dropoff_final', 
	'gapped', 'hsps_gapped', 'hsps_no_gap', 'hsps_prelim_gapped', 'hsps_prelim_gapped_attemped', 
	'ka_params', 'ka_params_gap', 'matrix', 'multiple_alignment', 'num_good_extends', 'num_hits', 
	'num_letters_in_database', 'num_seqs_better_e', 'num_sequences', 'num_sequences_in_database', 
	'posted_date', 'query', 'query_id', 'query_length', 'query_letters', 'reference', 'sc_match', 
	'sc_mismatch', 'threshold', 'version', 'window_size']

	# contents of a description in a record
	dir(r.descriptions[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'bits', 'e', 
	'num_alignments', 'score', 'title']

	# contents of an alignment in a record
	dir(r.alignments[0])
	['__doc__', '__init__', '__module__', '__str__', 'accession', 'hit_def', 
	'hit_id', 'hsps', 'length', 'title']

	dir(r.alignments[0].hsps[0])
	['__doc__', '__init__', '__module__', '__str__', 'align_length', 'bits', 'expect', 'frame', 
	'gaps', 'identities', 'match', 'num_alignments', 'positives', 'query', 'query_end', 
	'query_start', 'sbjct', 'sbjct_end', 'sbjct_start', 'score', 'strand']
"""

if __name__ == "__main__":
	main(sys.argv[1:], sys.stdout)

