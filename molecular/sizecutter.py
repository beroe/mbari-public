#! /usr/bin/env python
#
# version 2.1 - corrected error from GC content of unequal length sequences 19/oct/2012
#               changed GC counter to use set(), to avoid repeated letters
# version 2.0 - added feature to generate histogram from 0 to 100 17/jul/2012
# version 1.9 - changed return options 26/jun/2012
# version 1.8 - added trimming function, to only output first N letters 26/may/2012
# version 1.7 - moved all parameters to 'outvalue' to enable cutting of all outputs 11/may/2012
# version 1.6 - added option to count longest polyN 9/may/2012
# version 1.5 - minor cleanups of format 23/apr/2012
# version 1.4 - added gc counter 11/jan/2012
# version 1.3 - added n50 calculator with -n, and only-count option 9/jan/2012
# version 1.2 - converted inputs as argparse, rather than sys.argv 31/dec/2011
# version 1.1 - added options of size cutoff and print sequence output
#               also uses Bio fasta parser, rather than previous
#               dictionary-based approach 13/dec/2011
# version 1.0 - first version
#
# email wfrancis at mbari dot org with questions or comments

"""SIZECUTTER.PY
  Requires Bio library (biopython)

A simple program to output the length of fasta sequences:
	sizecutter.py filename.fasta > sizes.txt
To take the stdin from another program:
	head -n 100 filename.fasta | sizecutter.py -
For sequences longer or shorter, use -a or -b, respectively:
	sizecutter.py -a 1000 filename.fasta > longsizes.txt
	sizecutter.py -b 1000 filename.fasta > shortsizes.txt
To print out the fasta format sequences in use with the size filter:
	sizecutter.py -a 1000 -p filename.fasta > longsequences.fasta
To only display summary statistics, and not write out lengths:
	sizecutter.py -s filename.fasta
To additionally calculate the n50 and other values:
	sizecutter.py -n filename.fasta
Use fastq sequences with:
	sizecutter.py -f filename.fastq
To output the GC content in histogram form:
	sizecutter.py -s -l -q -g GC filename.fasta
"""
import sys
import os.path
from Bio import SeqIO
import argparse
import time

def main(argv, wayout):
	if not len(argv):
		argv.append("-h")

	# this handles the system arguments
	parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=__doc__)
	parser.add_argument('input_file', type = argparse.FileType('rU'), default = '-', help="fasta format file")
	parser.add_argument('-a', '--above', type=float, metavar='N', default=0.0, help="only count sequences longer than N")
	parser.add_argument('-b', '--below', type=float, metavar='N', default=1000000000.0, help="only count sequences shorter than N")
	parser.add_argument('-g', '--gc-content', metavar='GC', help="instead of size, print the count of N or N...")
	parser.add_argument('-r', '--repeats', metavar='A', help="instead of size, print the length of the longest polyN")
	parser.add_argument('-l', '--by-length', action='store_true', help="for -g or -r, output percent of length, rather than count; will affect most calculations")
	parser.add_argument('-n', '--n50', action='store_true', help="calculate the mean, median, n50 and n90 for the set")
	parser.add_argument('-p', '--print-seq', action='store_true', help="write sequences instead of sizes when cutoff is used")
	parser.add_argument('-f', '--fastq', nargs='?', const='fastq', default='fasta', help="use fastq format sequences")
	parser.add_argument('-s', '--summarize', action='store_true', help="only count, do not print output")
	parser.add_argument('-q', '--frequency', action='store_true', help="generate histogram of output, must use -l")
	parser.add_argument('-t', '--trim', type=int, metavar='N', default=0, help="print out the first N letters of sequence")
	args = parser.parse_args(argv)

	# all integers initialized
	seqcount = 0
	rawsum = 0
	# additional parameters
	formattype = args.fastq
	# this may need to be "fastq-sanger" or "fastq-illumina"

	inputfilename = args.input_file
	# alert user of non-unique letters in gc-content string
	getgc = args.gc_content
	if getgc:
		if len(getgc) != len(set(getgc)):
			print >> sys.stderr, "# ERROR: Redundant letters in list. ", time.asctime()
	# form regular expression
	getpolya = args.repeats
	if getpolya:
		import re
		repolya = re.compile((args.repeats[0])+"+")
		print >> sys.stderr, "# Finding repeats of %s: " % (args.repeats[0]) +time.asctime()
	# list for tracking total number of written bases
	seqsizes = []
	print >> sys.stderr, "# Parsing sequences on %s: " % (inputfilename.name) +time.asctime()
	# start main loop to iterate through sequence file
	for seq_record in SeqIO.parse(inputfilename, formattype):
		seqcount += 1
		seqlength = len(seq_record)
		# track total number of read bases
		rawsum += seqlength
		# for '-r' mode
		# uses regular expression for finding longest repeat out of list of all repeats
		if getpolya:
			polyresults = repolya.findall(str(seq_record.seq))
			try:
				outvalue = len(max(polyresults))
			# for cases where repeat does not occur once, and max of list fails
			except ValueError:
				outvalue = 0
		# for '-g' mode
		# for each unique letter in string of args.gc_content, count it, and sum all of the results
		elif getgc:
			outvalue = sum([str(seq_record.seq).count(i) for i in set(args.gc_content)])
		else:
			outvalue = seqlength
		# for '-l' option
		# if using by-length parameter, get percentage by length
		# this is meant for viewing individual transcripts
		# summary will not be accurate unless all sequences are the same length
		if args.by_length:
			outvalue = 100.0 * outvalue/float(seqlength)
		# for '-a' and '-b'
		# if sizes are within high and low cutoffs
		if (args.below > outvalue >= args.above):
			# make list of all sequences that match above criteria
			seqsizes.append(outvalue)
			if not args.summarize:
				if not args.print_seq:
					print >> wayout, outvalue
				else:
					# for '-t' option
					if args.trim:
						seq_record.seq = seq_record.seq[:args.trim]
					wayout.write(seq_record.format(formattype))
	# determine number of sequences within cutoff parameters, and output
	writecount = len(seqsizes)
	seqsum = sum(seqsizes)
	sumratio = float(seqsum)/rawsum
	print >> sys.stderr, "# Counted %d sequences: " % (seqcount), time.asctime()
	print >> sys.stderr, "# Wrote %d sequences: " % (writecount), time.asctime()
	print >> sys.stderr,"# Total input letters is:", rawsum
	print >> sys.stderr,"# Total output count is:", seqsum
	print >> sys.stderr,"# Ratio of out/in is:", sumratio
	# for '-q' option
	# if generating frequency histogram, and if seqsizes list is not empty
	# histogram is printed to stdout, so it can be written directly to file
	# -s option should be used
	if args.frequency and writecount:
		hd = {}
		# bin by intervals of 1 by converting floats to ints and counting
		for x in seqsizes:
			xi = int(x)
			# unseen values of xi are defaulted to 0
			hd[xi]=hd.get(xi,0)+1
		for x in range(101):
			print >> wayout, "%d\t%d" % (x,hd.get(x,0))
	# if finding the n50 and other stats, and if seqsizes list is not empty
	if args.n50 and writecount:
		n50, n90, ncountsum = 0,0,0
		print >> sys.stderr,"# Sorting sequences: ", time.asctime()
		seqsizes.sort()
		halfsum = seqsum//2
		ninesum = int(seqsum*.9)
		seqmean = float(seqsum)/writecount
		seqmedian = seqsizes[(writecount//2)]
		seqmin = min(seqsizes)
		seqmax = max(seqsizes)
		if seqsum:
			print >> sys.stderr,"# Average length is: %.2f" % (seqmean)
			print >> sys.stderr,"# Median length is:", seqmedian
			print >> sys.stderr,"# Shortest sequence is:", seqmin
			print >> sys.stderr,"# Longest sequence is:", seqmax
			for i in seqsizes:	
				ncountsum += i
				if ncountsum >= halfsum and not n50:
					n50 = i
					print >> sys.stderr,"# n50 length is: %d and n50 is: %d" % (ncountsum, n50)
				if ncountsum >= ninesum and not n90:
					n90 = i
					print >> sys.stderr,"# n90 length is: %d and n90 is: %d" % (ncountsum, n90)
		else:
			print >> sys.stderr,"# All lengths and parameters are: 0"
		print >> sys.stderr, "# Done: ", time.asctime()
		return seqcount, seqsum, seqmean, seqmedian, n50, n90
	else:
		return seqcount

if __name__ == "__main__":
	main(sys.argv[1:],sys.stdout)
