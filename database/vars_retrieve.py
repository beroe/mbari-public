#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
v 4.1 : Restructures and added simple SQL query function for import
v 4.04: Gave the getdivesummary function an option to return the string
v 4.03: Fixed --biolum parsing when no concept is provided
v 4.02: Changed output of --biolum to show NA for things not in KB
v 4.01: Fixed header when retrieving by -k and -p
v 4.0 : Added --biolum option
v 3.97: Filtering out depths > 4500 meters. ...
v 3.96: Remove miniROV dives, Generate SkipList
v 3.95: Added -f1 option to -p to print dive strings
v.3.94: Can use comments to download images... 
v 3.93: Improvements and robustness for --normalize
v 3.91: Adding -k to comment finder
v 3.9 : Added time at quantized depth for normalization.
v 3.83: Bug fix in overly optimistic recursion scheme
v 3.82: Handles strange unicode in --hurry mode better
v 3.81: Made --hurry mode work in combo with dive and PI constraints
v 3.8 : Added --hurry mode for simple but large concept-only queries
v 3.75: Added time for query processing to debug mode
v 3.74: Super sample mode --super gives all samples for old dives
v 3.73: Handles stray linefeed in Notes field
v 3.72: Added timeout message for db connection
v 3.71: Fixed glitch if running all feeding w/o dive
v 3.7:  Updated to tolerate Mini ROV dives
V 3.6 : Added option to get all associations
v 3.53: Fixed get PNG (-g) for category (-k) searches 
v 3.52: Make the parent concept search case insensitive 
v 3.51: Fix header names and change header separater to underscore 
v 3.5:  Retrieve parents of concept by I (TODO: return with each record)
v 3.43: Can constrain image retrieval by PI
v 3.42: Made it so dive constraint returns full set of fields
v 3.4:  Changed default use of wildcard in concept searches
v 3.32: fixed bug from last edit for simple concept retrieval
v 3.31: fixed retrieval of concepts hierarchy and format string
v 3.3:  Added retrieving samples or dive annotations by KB concept group
v 3.2:  shipboard version
v 3.1:  Improved handling of combinations of arguments.
V 3.0:  Refactored to use argparse with more control
v 2.2:  Added PI searching for dive summaries and samples
v 2.1:  Works with partial database for offsite access
V 2.0:  Added ability to download images by concept or dive number
v 1.47: Changed order of fields in output, added field number for cutting
v 1.46: Cleaned up output and improved library installation instructions
v 1.45: Added associations and reformatted output
v 1.4:  Added extraction of species from higher taxon
v 1.3:  Handle sample numbers in V3875-D5 format
v 1.2:  Flag to perform different queries at run time
v 1.1:  Parses the V2132 format. Can also take dive number w/o vehicle
  [haddock at MBARI dot org]

Will find either a concept or a list of samples from a dive.

To find a concept run with a COMMA-separated list of concept names (no space).
If there are spaces in the concept, enclose the whole thing in quotes: 
   'Aegina,Aegina citrea,Aegina rosea,Aegina sp. 1'
   
For wildcards, put % where you want wildcards and enclose in quotes: '%salp%'

To find samples from a dive, run with the appropriate flag as true,
and send in a space- or comma-delimited list of dive numbers.

NOTE: Comma-separated lists cannot have spaces too...

TODO: Change comment implementation to use a CommentString that gets 
      inserted into existing functions (image, etc)
	  
You can search in these ways:

 -s  : samples from a dive
 -a  : associations between groups
 -c  : get all annotations of concepts for all dives
        WARNING: Genus along will not get the species. 
        Use -k option or put full species in quotes
        NOTE: You can also put % after part of the name to use wildcards
 -k  : species included within a higher taxon
 -i  : images from a dive (specify -p or -d)
 -g  : Retrieve PNG images, without overlay, instead of JPEG
 -l  : List the images links only, don't download
 -d  : dive summary for dives [or to specify dives]
 -f  : print as formatted report [mainly for associations and -k option]
 -b  : print debug output
 -w  : use wildcards in concept searches (Periphyll will match Periphyllopsis)
 -z  : pull depth information for a dive
 
 --flyer    : search on internal server of Western Flyer
 --anela    : Find all feeding associations. Can specify dive or PI
                  Two different format options: summary [default] or -f1 for full table 
                  Does not return unique records in summary table. Must use vars_unique.py
 --super    : On old dives, a sample reference is not always there. 
                  Use this to search for all "sample" Links
 --hurry    : For concepts with many results (>20,000) will print more quickly
 --normalize: Generate normalization matrix for dive or ALL dives if unspecified (?)
 --comment  : Search in the Link Name, Value, ToConcept for terms. 
              Requires a concept with -c or -k. Use -k Eukaryota to search all animals
 --biolum   : Retrieve dictionary of Concept:is-bioluminescent
                   Use with -c or -k to look up taxa
				   Format with -f1 to get python dictionary
 
For each of those, you can use in combination with 
   -p [PI name] or -d [dive range] and/or -c [concept name].

   -p     : get dive summaries by PI (or multiple PIs with commas)
             to limit to the most recent X dives, put ,X at the end...
   -d     : provide a range of dives for a search
          : d by itself without other flags above gets a dive summary
   -a     : get all associations between two or more groups, separated by +
             secret option: put 1 at the end for full table (nothing is summary)
   -k     : get all species for a higher taxon (takes only one name)
             secret option: put -f 1 at the end for quoted csv, -f 2 for full table
             Note: by itself this just lists sub-concepts, and doesn't retrieve
             However, used in conjunction with -s or -i, it will retrieve records
 -s -p    : get sample lists from listed PIs. Use ,X to limit to recent X dives
 -s -k    : get samples matching a broader KB concept
 -d -k XXXX -d YYYY   : get all annotations for KB group XXXS on a dive YYYY
 
Use grep, cut and sort to extract certain columns.

For associations, default is a summary table. Use -f1 to get all the records.  
Usage examples: 
	vars_retrieve.py -s D422 R500          # Get all samples from Doc Ricketts 422 and 500
	vars_retrieve.py -d D422,v360          # Get dive summaries for two dives
	vars_retrieve.py -c Aulacoctena,"Bathyctena chuni"
	vars_retrieve.py -i Paraphronima
	vars_retrieve.py -i -c Bathyctena
	vars_retrieve.py -i -d T412                    # get all images from T412
	vars_retrieve.py -g -d T412 -c Aegina          # get all Aegina images in PNG format from T412
	vars_retrieve.py -d v{2770..2880}              # get a dive summary for all dives between those two numbers
	vars_retrieve.py -k Narcomed,Scyphozo          # All species for both Narcos and Scyphos 
	vars_retrieve.py -u Aegina                     # Search Up the tree for parents of concept  
	vars_retrieve.py -a Narcome,Scyphozoa+Hyperiid # Associations between Narcos and Amphipods
	vars_retrieve.py -p Haddock,Robison,10         # Get the most recent 10 dives each by Haddock and Robison
	vars_retrieve.py -c Erenna% -p Haddock,10 -i   # Get Erenna spp images for the most recent Haddock 10 dives 
	vars_retrieve.py -s -p Haddock,2               # Get the sample list from the two most recent Haddock dives
	vars_retrieve.py -d T{840..847} -k Lobata      # Get all lobate ctenophores annotated on dives T840 through T847
	vars_retrieve.py -d T841 -c Bathocyroe         # Exact concept Bathocyroe (Genus will not find genus+sp
	vars_retrieve.py -s -d D326 -k Ctenophora      # Get all ctenophore samples from dive Doc 326
	vars_retrieve.py -d v{3500..4000} | sort -k 4  # Get Ventana dives between 3500 and 4000 sorted by number
	vars_retrieve.py -c Macropinna% | cut -f1,2,22 | sort -u # Get a summary of dives where Macropinna was seen
	vars_retrieve.py -u squalus -f2                # Show the parent group classification for dogfish
	vars_retrieve.py --anela -p Haddock,10         # Return summary of feeding associations on last 10 Haddock dives
	vars_retrieve.py --anela -d D505,V829 -f1      # Return complete records of feeding associations.
	vars_retrieve.py -c Beroe -p Haddock --hurry   # Get all Beroe from Haddock dives, with performance boosting
	vars_retrieve.py --comment damaged -k Ctenophora -i  # Get all images of damaged ctenophores
	vars_retrieve.py --normalize -p Haddock,10     # Create a histogram of depth bins showing effort for dives
	
Requires: pymssql module, freetds-dev, and unixODBC 
Installation instructions are a comment in the source code directly below...

"""
# main() code is at the bottom

# TODO: Implement wildcard in getconceptbydive and getimages
# instead of if-then, use +["","%"][Wild]

# TODO: Get ctd data from EXPD: select * from Rovctd
# Options: 
# SELECT divestartdtg, diveenddtg FROM Dive WHERE rovname = ? AND divenumber = ?` 
# SELECT DatetimeGMT, t, s, o2, o2Flag, o2alt, o2altFlag, light, p FROM <rovname>RovctdData WHERE DatetimeGMT BETWEEN <divestartdtg> AND <diveenddtg>
# <rovname>CleanNavData

InstallString="""
# IF YOU ARE SEEING THIS, THEN REQUIRED LIBRARIES WERE NOT FOUND.
# INSTALLATION: Do all these steps and commands to install the 
# pre-requisites for OSX, in this order. Commands refer to the Terminal
#
# If it asks you for a password, it will be your normal login password
#     1. If you don't have gcc (type "which gcc" and see if it is found) then you will need
#        the Apple Command Line Tools
         You should be able to get these by typing
           gcc
         in a terminal window, and if you don't have them, it will pop up a prompt 
         asking if you want to install them. 
#     2. Install HomeBrew with these exact commands: 
#         ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
#         sudo chown -R $USER /usr/local
#         brew doctor
#     3. Install pip with this exact command:
#         sudo easy_install pip
#     4. Now you can install the packages:
#          brew install unixodbc
#          brew install --with-unixodbc freetds
#          sudo pip install pymssql

# INTALLATION FOR UBUNTU / LINUX:
 sudo apt-get install unixodbc
 sudo apt-get install freetds-dev
 sudo apt-get install python-pip    # May not be necessary on some systems
 sudo apt-get install python-dev    # May not be necessary on some systems
 sudo pip install pymssql   
"""



def get_options():
	parser = argparse.ArgumentParser(usage = __doc__)
	parser.add_argument("-b", "--debug",    action="store_true", dest="DEBUG", default=False, help="Print debug info")
	parser.add_argument("-c", "--concept",    action="store", dest="Concept", help="Search for concept in comma-separated list")
	parser.add_argument("-l", "--listimages",    action="store_true", dest="ListImages", default=False, help="Search for images of listed concepts")
	parser.add_argument("-i", "--images",    action="store_true", dest="GetImages", default=False, help="Search for images of listed concepts")
	parser.add_argument("-g", "--png",    action="store_true", dest="GetPNG", default=False, help="Download PNG instead of JPEG")	
	parser.add_argument("-d", "--dives",    action="store", dest="Dives", help="Provide dive numbers for other searches -i -c -r")
	parser.add_argument("-p", "--pi",    action="store", dest="Pis", help="Search for dives by PI")
	parser.add_argument("-s", "--samples",    action="store_true", dest="Samples", default=False, help="Retrieve samples from dive or PI (with -p)")
	parser.add_argument("-k", "--knowledge",    action="store", dest="Knowledge", help="Get concepts from Knowledgebase")
	parser.add_argument("-u", "--upladder",    action="store", dest="UpConcept", help="Get Parent Hierarchy from Knowledgebase")
	parser.add_argument("-a", "--association",    action="store", dest="Association", default=False, help="Get associations between Concept+Concept")
	parser.add_argument("-f", "--format",    action="store", dest="Format", default=0, help="Format for printed reports")
	parser.add_argument("-w", "--wildcard",    action="store_true", dest="Wildcard", default=False, help="Use Wildcards in Searches")
	parser.add_argument("-z", "--getdepth",    action="store_true", dest="GetDepth", default=False, help="Get dive depth profile")
	parser.add_argument("--super",    action="store_true", dest="Supersample", default=False, help="All 'sampled' annotations, for old records")
	parser.add_argument("--anela",     action="store_true", dest="Anela", default=False, help="Find all feeding associations (per dive)")
	parser.add_argument("--hurry",     action="store_true", dest="Hurry", default=False, help="Print Rows as they come...")
	parser.add_argument("--comment",     action="store", dest="Comment", help="Find records for concept with specified comment")
	parser.add_argument("--normalize",    action="store_true", dest="Normalize", default=False, help="Generate matrix of seconds per depth for dives or PI")
	parser.add_argument("--flyer",    action="store_true", dest="Flyer", default=False, help="Run on Flyer internal database")
	parser.add_argument("--biolum",    action="store_true", dest="Biolum", default=False, help="Find Bioluminescence")
	parser.add_argument("--useship",    action="store_true", default=False, help="Use ship nav for ROV summaries")
	
	parser.add_argument("Args", nargs='*')
	options = parser.parse_args()
	return options

def execquery(query, mydatabase="VARS",hurry=False,keeplist=False):
	from base64 import b64decode
	guest='==AdzxEdzFzVXVmT'[::-1]
	guest2='xADMxUjOnJ3bukmchJWbuUmbvlGZ'
	serverlookup = {
		"EXPD"	 : "solstice.shore.mbari.org",
		"VARS"	 : "equinox.shore.mbari.org",
		"VARS_KB": "equinox.shore.mbari.org"
	}
		
	
	# grab the right server name, depending on what database is being used...
	username = "everyone"
	pw = "guest" 

	if Opt.Flyer:
		servername = 'alaskanwind.shore.mbari.org'
	elif Onsite:
		servername = serverlookup[mydatabase]
	else:
		if mydatabase == "EXPD":
			servername = serverlookup[mydatabase]
		else:
			servername = b64decode(guest2[::-1])

	try:
		conn = pymssql.connect(servername, loc_red()[0], loc_red()[1], database=mydatabase, login_timeout=7, as_dict=False)
	except pymssql.OperationalError:
		sys.exit("\n   >>> Database connection timed out. May require VPN.<<<\n")
	cur = conn.cursor()

	NumRecords=0

	if Opt.DEBUG:
		print >> sys.stderr, "Starting query...",time.asctime()
	cur.execute(query)
	if Opt.DEBUG:
		print >> sys.stderr, "Finished query...",time.asctime()
	outstr=""
	# OutL = list(cur)
	# print OutL
	LoL = []
	for row in cur:
		if keeplist:
			LoL.append([x for x in row])
			outstr=LoL
		elif not hurry:
			NumRecords += 1
			strlist = ["%s"%x for x in row]
			outstr += "\t".join(strlist)
			outstr += "\n"
		else:
			NumRecords += 1
			try:
				strlist = ["{}".format(x) for x in row]
				print "\t".join(strlist)
			except UnicodeEncodeError:
				strlist = [unicode(x).encode('unicode_escape') for x in row]
				print "\t".join(strlist)
			outstr = ""

	if Opt.DEBUG:
		print >> sys.stderr, "Formatted query..",time.asctime()

	conn.close()
	return NumRecords,outstr

def simpleexecquery(query, mydatabase="VARS"):
	'''no overhead, returns a raw list of the results'''
	import pymssql
	serverlookup = {
		"EXPD"	 : "solstice.shore.mbari.org",
		"VARS"	 : "equinox.shore.mbari.org",
		"VARS_KB": "equinox.shore.mbari.org"
	}	
	
	# grab the right server name, depending on what database is being used...
	username = "everyone"
	pw = "guest" 
	servername = serverlookup[mydatabase]
	try:
		conn = pymssql.connect(servername, username, pw, database=mydatabase, login_timeout=7, as_dict=False)
	except pymssql.OperationalError:
		sys.exit("\n   >>> Database connection timed out. May require VPN.<<<\n")
	cur = conn.cursor()

	cur.execute(query)
	LoL = []
	for row in cur:
		strlist = ["{}".format(x) for x in row]
		LoL.append( "\t".join(strlist))		
		# LoL.append([x for x in row])
		# outstr=LoL
	conn.close()
	return LoL


def ptoz(pstring,lat=35):
	'''Convert pressure to depth. 
	From http://www.seabird.com/document/an69-conversion-pressure-depth'''
	import math
	p = float(pstring)
	x = math.sin(lat/57.29578)**2
	g =  9.780318 * ( 1.0 + ( 5.2788e-3  + 2.36e-5  * x) * x ) + 1.092e-6  * p
	z = ((((-1.82e-15  * p + 2.279e-10 ) * p - 2.2512e-5 ) * p + 9.72659) * p) / g
	return "{:.1f}".format(z)

def getdivebypi(DiveList,ReturnThem=False):
	""" Using the name DiveList just so we don't have to rename, but it is really PI List
	    Optional last argument as a digit limits the number of returns"""
	
	SQuery = """
	SELECT DISTINCT {1}
		  chiefscientist, shipname, rovname,  divenumber, avgrovlat, avgrovlon, divestartdtg, maxpressure
	FROM  dbo.DiveSummary
	WHERE (chiefscientist like '%{0}')  {2}
	"""
	#TODO: Add date info?
	
	SHead = "ChiefScientist	ShipName	RovName	DiveNumber	AvgROVLatitude	AvgROVLongitude	DiveStartTime	MaxPressure"
	if Opt.Format=='1':
		ReturnThem = True

	if not ReturnThem:
		print SHead
	if "," in DiveList:
		DiveList = DiveList.split(",") 
	else:
		DiveList = [DiveList]
	Limit = ''
	Tail = ''
	if DiveList[-1].isdigit():
		Limit = "TOP " + DiveList[-1]
		DiveList = DiveList[:-1]
		Tail = 'ORDER BY divestartdtg DESC'
		
	NumFound = 0
	TotalFound = 0
	DList = []

	
	for Dive in DiveList:
		SQ = SQuery.format(Dive,Limit,Tail)
		# print SQ
		NumFound,Outstr = execquery(query = SQ, mydatabase="EXPD")
		TotalFound += NumFound
		if ReturnThem:
			for Line in Outstr.split('\n'):
				Fields = Line.split("\t")
				if len(Fields)>3:
					DList.append(Fields[2:4])
		else:
			print Outstr.rstrip("\n")

	if Opt.Format=='1':
		Dout = [ x[0].upper() + y for x,y in DList ]
		print >> sys.stderr, ",".join(Dout)
	if ReturnThem:
		return DList
	sys.stderr.write("## Found %s dives by PI...\n" % (TotalFound))

	
	
def getrovdepth(ROV,ROVnumber):
	ROVletter = ROV[0].upper()
	LongDict = dict(zip(list("TVRDM"),["Tiburon","Ventana","DocRicketts","DocRicketts","MiniROV"]))
	ShortDict = dict(zip(list("TVRDM"),["tibr","vnta","docr","docr","mini"]))
	shortrov = ShortDict[ROVletter]
	longrov  = LongDict[ROVletter]
	
	# NOTE: Some ctds report every 2 seconds and some 2 second...!
	# Syntax for doing it as one query
	# SELECT  EpochSecs,[Depth] FROM VentanaCleanNavData,
	#   (SELECT divestartdtg AS startDate, diveenddtg AS endDateFROM Dive
	#    WHERE rovname = 'vnta' AND divenumber = 3903) AS TimeBounds
	# WHERE DatetimeGMT BETWEEN TimeBounds.startDate AND TimeBounds.endDate
	
		
	TQ = '''SELECT divestartdtg, diveenddtg FROM Dive 
	     WHERE rovname = '{shortrov}' AND divenumber = {divenum}'''.format(shortrov=shortrov,divenum=ROVnumber)

	if Opt.DEBUG:
		print >> sys.stderr, TQ
		 
	# RUN AN INITIAL QUERY TO GET THE TIMES
	n,tvals = execquery(TQ,mydatabase="EXPD",keeplist=True)
	# Returns time tuple. Need string
	if not tvals:
		print "# NO ENTRY for start times: {}".format(ROV[0].upper()+ROVnumber)
		return 0,[]

	qstart,qend = tvals[0]
	
	# SHOULDN"T BE FASTER BUT FOR SOME REASON IT IS IN THE GUI



	SQ="""SELECT EpochSecs,[Depth] FROM {longrov}{clean}NavData where DatetimeGMT BETWEEN
	'{tstart}' AND '{tend}'"""

	Query = SQ.format(longrov=longrov,clean="Clean",tstart=qstart.strftime(r'%D %r'),tend=qend.strftime(r'%D %r'))
	if Opt.DEBUG:
		print >> sys.stderr, Query
	
	n,ctd = execquery(Query,mydatabase="EXPD",keeplist=True)

	if not ctd:
		Query = SQ.format(longrov=longrov,clean="Raw",tstart=qstart.strftime(r'%D %r'),tend=qend.strftime(r'%D %r'))
		print "# Trying Raw data for {}".format(ROV[0].upper()+ROVnumber)
	n,ctd = execquery(Query,mydatabase="EXPD",keeplist=True)
		
	# raw query return is a tuple like (datetime.datetime(2016, 2, 18, 22, 17, 56), 1455833876, 3.4700000286102295),
	# if Opt.DEBUG:
	# 	print "CTD0  ",ctd[0]
	# 	print "CTDend",ctd[-1]
	# 	print "TYPE:",type(ctd)
	
	return n,ctd
	
def quantizedepths(ROV,DiveNum,binsize=50):
	""" THIS FUNCTION could be made testable"""
	
	DepthD = {}
	n,ctd = getrovdepth(ROV,DiveNum)
	
	# Round to integer, but do we want to quantize to 10 (50?) meters?
	if len(ctd) < 10:
		print >> sys.stderr,  "# BAD CTD {}".format(ROV+DiveNum)
		if Opt.DEBUG:
			print ctd
		return {}
	try:
		Times,Depths = zip(*ctd)
	except ValueError:
		print "# CTD VALUEERROR {}".format(ROV+DiveNum)
		return {}
		
	starttime = Times[0] # Is this ever NaN??
	endtime = Times[-1]
	
	if Opt.DEBUG:
		print >> sys.stderr, "DEPTHS",min(Depths),max(Depths)
		print >> sys.stderr, "TIMES", Times[0],'\n',Times[-1]
	if Opt.DEBUG:
		print >> sys.stderr, "Binning data..",time.asctime()
	
	binned = [int(y - ((y - 1+ binsize/2.0) % binsize) + binsize/2.0 -1) for y in Depths if (y < 4200)]
	
	interval = int(round((endtime - starttime)/float(len(ctd))))
	checkinterval = int(Times[3] - Times[2])
	if interval > 11 or abs(interval - checkinterval)>1:
		print "# INTERVAL ERROR: Total {}. Diff {} | {}".format(interval,checkinterval,ROV+DiveNum)
		print >> sys.stderr,  "# INTERVAL ERROR: Total {}. Diff {} | {}".format(interval,checkinterval,ROV+DiveNum)
		return {}	
	
	maxd = max(binned)

                          # Fri Mar 18 22:22:47 2016  | vnta 2665  766 remain...
	print >> sys.stderr, "#      Interval {}          | {}".format(interval,ROV[0].upper()+DiveNum)

	for d in range(0,maxd+1,binsize):
		DepthD[d] = binned.count(d) * interval
	if Opt.DEBUG:
		print >> sys.stderr,  DepthD
	return DepthD

def texthistogram(dlist=[],tlist=[],DepthD = {},WindowWidth=50,DiveDict={}):
	"""Given data dictionary or parallel lists of Depth: Count, make a histogram"""
	if DepthD:
		Dtuple = zip(DepthD.values(),DepthD.keys(),)
		Dtuple.sort()
		tlist,dlist = zip(*Dtuple)
	Multiplier = float(WindowWidth) / max(tlist) ## FIXME
	for d,t in sorted(zip(dlist,tlist)):
		print >> sys.stderr,  "{:>6d} : {}|{}".format(d,"=" * int(round(t* Multiplier)),t)
	if DiveDict:
		MasterList = [item for sublist in DiveDict.values() for item in sublist]
		print "# Collated {} dives with {} values on {}".format(len(MasterList),sum(tlist),time.ctime())


def generatenormalization(DiveList='',PIList='',BinSize=10):
	SkipList = []
	GoodList = []
	DiveDict = parsediveoptions(DiveList=DiveList,PIList=PIList)
	if not DiveDict:
		sys.exit("You must provide a dive list or PI list with -d or -p")
	try:
		del DiveDict['mini']
	except KeyError:
		pass
		
	starttime=time.ctime()
	print >> sys.stderr, "# ------- Starting query {}".format(starttime)
	MasterD = {}
	MasterList = [item for sublist in DiveDict.values() for item in sublist]
	LenD = len(MasterList)
	
	for ROV in DiveDict:	
		for DiveNum in DiveDict[ROV]:
			LenD -= 1
			sys.stderr.write("#  {2}  | {0} {1}  {3} remain...\n".format(ROV,DiveNum,time.ctime(),LenD))
			DepthD = quantizedepths(ROV,DiveNum,BinSize)
			if DepthD:
				GoodList.append(ROV[0].upper()+DiveNum)
				for D in DepthD:
					MasterD[D] = MasterD.get(D,0) + DepthD[D]
			else:
				SkipList.append(ROV[0].upper()+DiveNum)
	if MasterD:
		texthistogram(DepthD=MasterD,DiveDict = DiveDict)
	sys.stderr.write("#  Started query {}...\n".format(starttime))
	sys.stderr.write("# Finished query {}...\n".format(time.ctime()))
	print >> sys.stderr, "# SKIPLIST:", ",".join(SkipList)
	
	return MasterD
		
def getmyip(Debug=False):
	import socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		s.connect(("8.8.8.8",80))
		ipl = s.getsockname()
		if Debug:
			print >> sys.stderr, ("IP LIST:"),ipl
		ip = ipl[0]
		s.close()
	except socket.error:
		sys.stderr.write("--> No Network, Debug mode <--\n")
		ip = '127.0.0.1'
	if Debug:
		sys.stderr.write("IP ADDRESS: %s\n" % ip)
	return ip,ip.startswith('134.89.')

def getmyip2(Debug=False):
	import os
	import socket
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		s.connect(("8.8.8.8",80))
		ipl = s.getsockname()
		if Debug:
			print >> sys.stderr, ("IP LIST:"),ipl
		ip = ipl[0]
		s.close()
	except socket.error:
		sys.stderr.write("--> No Network, Debug mode <--\n")
		ip = '127.0.0.1'
	if Debug:
		sys.stderr.write("IP ADDRESS: %s\n" % ip)
	if os.name == "posix":
		Subp = subprocess.Popen(["ifconfig"], stdout=subprocess.PIPE)
		Result = Subp.stdout.readlines()
		if "ppp0" in Result[-4] and "134.89" in Result[-2]:
			if Debug:
				sys.stderr.write("VPN Suspected... Trying OnSite\n")
			ip = "134.89.1.1"		
	return ip,ip.startswith('134.89.')
	
def loc_red():
	from base64 import b64decode
	e_lou = 'ZXZlcnlvbmU='
	e_lop = 'Z3Vlc3Q='
	e_loh = 'nJ3bukmchJWbuUmbvlGZ'
	# e_lop = '==AdzxEdzFzVXVmT'[::-1]
		
	return (b64decode(e_lou),b64decode(e_lop))
	
def parsedivenumbers(Dive,shortname=False):
	Dive = Dive.strip()
	DiveDict = dict(zip(list("TVRDM"),["Tiburon","Ventana","Doc Ricketts","Doc Ricketts","MiniROV"]))
	if shortname:
		DiveDict = dict(zip(list("TVRDM"),["tibr","vnta","docr","docr","mini"]))
	if "-" in Dive:
		DFields = Dive.split("-")
		Dive = DFields[0]
		Sample = DFields[1]
	else:
		Sample = ""
	if Dive[0].isdigit():
		Veh= "%"
		Num = Dive.strip()
	else:
		Veh = DiveDict[Dive[0].upper()]
		Num = Dive[1:].strip()
	return Veh,Num,Sample


def divelisttodict(DiveList=[],PreList=False,Short=False):
	DiveDict={}
	RovDict = dict(zip(["tibr","vnta","docr","mini"],["Tiburon","Ventana","Doc Ricketts","MiniROV"]))
	for Dive in DiveList:
		if PreList:
			ShortName,DiveNum = Dive
			if Short:
				ROVName = ShortName
			else:
				ROVName = RovDict[ShortName]
		else:
			ROVName,DiveNum,_ = parsedivenumbers(Dive,shortname=Short)
		DiveDict[ROVName] = DiveDict.get(ROVName,[])+[DiveNum]
	return DiveDict
	


def getparentconcepts(conceptname,style=1):
	QueryString = """WITH org_name AS 
    (
        SELECT DISTINCT parent.id AS parent_id, parentname.ConceptName as parent_name,
            child.id AS child_id, childname.ConceptName as child_name
        FROM
            Concept parent RIGHT OUTER JOIN 
            Concept child ON child.ParentConceptID_FK = parent.id LEFT OUTER JOIN
            ConceptName childname ON childname.ConceptID_FK = child.id LEFT OUTER JOIN
            ConceptName parentname ON parentname.ConceptID_FK = parent.id
        WHERE
            childname.NameType = 'Primary' AND
            parentname.NameType = 'Primary'
    ), jn AS (   
        SELECT parent_id, parent_name, child_id, child_name
        FROM org_name 
        WHERE UPPER(child_name) = UPPER('{}')
        UNION ALL 
            SELECT C.parent_id, C.parent_name, C.child_id,C.child_name 
            FROM jn AS p JOIN org_name AS C ON C.child_id = p.parent_id
    ) 
    SELECT DISTINCT 
        jn.parent_id,
        jn.parent_name
    FROM jn ORDER BY 1;
    """.format(conceptname)
	if Opt.DEBUG:
		print >> sys.stderr, QueryString
	NumRecs, ParentString = execquery(QueryString, mydatabase="VARS_KB")
	if style != 1:
		WholeList = [z.split("\t") for z in ParentString.split("\n") if len(z)>=2]
		Parents = [y for x,y in WholeList if x not in ["1","90","91"]]
		if Parents:
			ParentString = ",".join(Parents)
	return ParentString

def getconceptfromKB(higher_taxa,style=1):
	""" Retrieve all subordinate taxonomic concepts from the knowledgebase
 Style 1 (default) is quoted and comma delimited. 
 Style 0 is a list with one concept per line
 Style 2 is the full table with concept numbers and higher-order classes
 """
	ConString = """ parent_name LIKE '%{}%' """
	Splits = higher_taxa.split(",")
	
	if len(Splits) > 1:
		ConList = Splits
	else:
		ConList = [higher_taxa]
	SearchNames = ""
	outstr=""
	NumCons = 0
	delimit=["\n",", ",""][style]  # zero gets newline
	for Con in ConList:
		# Capitalize concept search
		# if searching for species, this could be problematic
		Con = Con[0].upper() + Con[1:]
		Constraint = ConString.format(Con.strip())
		# print "CONSTRAINT: ", Constraint
	
		# Using knowledgebase (VARS_KB), select all the species for a group
		Cquery = """
   WITH org_name AS (
        SELECT DISTINCT
            parent.id AS parent_id, parentname.ConceptName as parent_name,
            child.id AS child_id, childname.ConceptName as child_name
        FROM
            Concept parent RIGHT OUTER JOIN 
            Concept child ON child.ParentConceptID_FK = parent.id LEFT OUTER JOIN
            ConceptName childname ON childname.ConceptID_FK = child.id LEFT OUTER JOIN
            ConceptName parentname ON parentname.ConceptID_FK = parent.id
        WHERE
            childname.NameType = 'Primary' AND
            parentname.NameType = 'Primary' ), 
   jn AS (   SELECT            parent_id, parent_name, child_id, child_name FROM org_name 
       WHERE ({}) 
       UNION ALL SELECT C.parent_id, C.parent_name, C.child_id, C.child_name FROM jn AS p 
       JOIN org_name AS C ON C.parent_id = p.child_id ) 
   SELECT DISTINCT jn.parent_id, jn.parent_name, jn.child_id, jn.child_name 
    FROM jn ORDER BY 1;
   """.format(Constraint)
		if Opt.DEBUG:
			print >> sys.stderr, Cquery
		NumRecs, SpeciesList = execquery(Cquery, mydatabase="VARS_KB")
		SearchNames += " and ".join(Con)
		NumCons += NumRecs
		sys.stderr.write("## Found %d concepts in hierarchy for query of %s\n"%(NumRecs+1,Con))
		if style == 2:
			outstr = SpeciesList
			break
		try:
			# Get the full name of the query to include it too, e.g. Chrysaora and not just speciesprint
			if style == 1:
				FirstName = "'{}'".format(SpeciesList.split("\t")[1])
				SpNames = [ "'{}'".format(F.split("\t")[3]) for F in SpeciesList.rstrip().split("\n")]
			else:
				FirstName = SpeciesList.split("\t")[1]
				SpNames = [ F.split("\t")[3] for F in SpeciesList.rstrip().split("\n")]
		except IndexError:
			# sys.stderr.write("No lower concepts found for {}\n".format(higher_taxa))
			if style == 1:
				FirstName = "'{}'".format(Con)
			else:
				FirstName = Con
			SpNames = []
			
		if outstr:
			head=delimit
		else:
			head=""
		outstr += head + delimit.join([ FirstName ] + SpNames)

	return NumCons,outstr

def allassociations(DiveList='',PIList='',PreList=False,Style=0,Debug=False):
	'''This is the --anela option, to get ALL feeding associations between concepts '''
	
	SQuery = '''SELECT DISTINCT RovName, DiveNumber,Depth, ConceptName,Latitude, Longitude, Temperature, Oxygen, Salinity,
	RecordedDate AS DateTime24, Image, TapeTimeCode, Zoom,
	ObservationID_FK AS obsid, LinkValue, linkName,
	VideoArchiveSetID_FK AS vasid, ShipName, Associations,
	videoArchiveName, CameraDirection,
	ChiefScientist, FieldWidth, Replace(Notes,CHAR(10),' '), Observer,ToConcept
	FROM Annotations
	WHERE (ObservationID_FK IN (SELECT ObservationID_FK FROM Annotations WHERE (  
          (( LinkName = 'eating' )  OR ( LinkName = 'feeding' ) 
           OR ( LinkName = 'feeding-association-predator-of') 
           OR ( LinkName = 'feeding-association-prey-of')) ) )
         {}  )
           ORDER BY DateTime24,TapeTimeCode ; '''

	
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		PreList = True
	elif DiveList and not PreList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if DiveList:
		DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
		for ROV in DiveDict:
			DiveNums =  "','".join(DiveDict[ROV])			
			sys.stderr.write("## Getting associations from {0} {1}...\n".format(ROV,DiveNums))
			if not Style:
				print >> sys.stderr, "## SUMMARY MODE. For full output, use -f1 for formatting."
			ExtraString = "AND (RovName like '{}') AND (DiveNumber IN ('{}') ) ".format(ROV,DiveNums)
			SQ = SQuery.format(ExtraString)
			if Debug:
				print >> sys.stderr, "## DIVE Query:\n", SQ
			NumFound, Outstr = execquery(SQ, mydatabase="VARS")
			TotalFound += NumFound
			AllOut += Outstr
		if TotalFound == 0 and not ('%' in SampleString):
			sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	else:
		SQ = SQuery.format("")
		if Debug:
			print >> sys.stderr, "## NO DIVE Query:", SQ
		TotalFound, AllOut = execquery(SQ, mydatabase="VARS")

	Fields = """RovName\tDiveNumber\tDepth\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\t\
	RecordedDate\tImage\tTapeTimeCode\tZoom\tObservationID\tLinkValue\tLinkName\t\
	VideoArchiveSetID\tShipName\tAssociations\tvideoArchiveName\tCameraDirection\t\
	ChiefScientist\tFieldWidth\tNotes\tObserver\tToConcept"""
	
	# SHead = "DateTime24	RovName	DiveNumber	ConceptName	Depth	Latitude	Longitude	TapeTimeCode	Observer	Link"
	L = Fields.split("\t")
	while '' in L:
		L.remove('')
	NumFields = "\t".join(["".join([j,"_",str(i+1)]) for i,j in enumerate(L)])		

	if __name__== "__main__" and Style:
		print NumFields
	return TotalFound,AllOut
	
def findwithcomment(Comment='',Concept = '',DiveList='',PIList='',PreList=False,Style=0,Debug=False):
	''' Look for concepts with a particular linked value. To search on all concepts, use -k Eukaryota '''
	
	SQuery = '''SELECT DISTINCT RovName, DiveNumber,Depth, ConceptName,Latitude, Longitude, Temperature, Oxygen, Salinity,
	RecordedDate AS DateTime24, Image, TapeTimeCode, Zoom,
	ObservationID_FK AS obsid, LinkValue, linkName,
	VideoArchiveSetID_FK AS vasid, ShipName, Associations,
	videoArchiveName, CameraDirection,
	ChiefScientist, FieldWidth, Replace(Notes,CHAR(10),' '),
	Observer,ToConcept
	FROM Annotations
	WHERE ( {concept} AND (ObservationID_FK IN (SELECT ObservationID_FK FROM Annotations WHERE (  
           ((LinkName Like '%{comment}%' ) OR ( LinkValue LIKE '%{comment}%' ) or (ToConcept LIKE '%{comment}%') )
         {extra}    ))))
           ORDER BY DateTime24,TapeTimeCode ; '''
	if "," in Concept:
		ConceptString = "(ConceptName IN ({}))".format(Concept)
	else:
		ConceptString = "(ConceptName LIKE '{}')".format(Concept.lower())	
		
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		PreList = True
	elif DiveList and not PreList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if DiveList:
		DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
		for ROV in DiveDict:
			DiveNums =  "','".join(DiveDict[ROV])			
			sys.stderr.write("## Getting commented from {0} {1}...\n".format(ROV,DiveNums))
			ExtraString = "AND (RovName like '{}') AND (DiveNumber IN ('{}') ) ".format(ROV,DiveNums)
			SQ = SQuery.format(concept=ConceptString,comment=Comment,extra=ExtraString)
			if Debug:
				print >> sys.stderr, "## DIVE Query:", SQ
			NumFound, Outstr = execquery(SQ, mydatabase="VARS")
			TotalFound += NumFound
			AllOut += Outstr
		if TotalFound == 0 and not ('%' in ConceptString):
			sys.stderr.write("If unexpected zero results, make sure to includ both a concept and comment\n")
	else:
		SQ = SQuery.format(concept=ConceptString,comment=Comment,extra="")
		if Debug:
			print >> sys.stderr, "## NO DIVE Query:", SQ
		TotalFound, AllOut = execquery(SQ, mydatabase="VARS")

	Fields = """RovName\tDiveNumber\tDepth\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\t\
	RecordedDate\tImage\tTapeTimeCode\tZoom\tObservationID\tLinkValue\tLinkName\t\
	VideoArchiveSetID\tShipName\tAssociations\tvideoArchiveName\tCameraDirection\t\
	ChiefScientist\tFieldWidth\tNotes\tObserver\tToConcept"""
	
	# SHead = "DateTime24	RovName	DiveNumber	ConceptName	Depth	Latitude	Longitude	TapeTimeCode	Observer	Link"
	L = Fields.split("\t")
	while '' in L:
		L.remove('')
	NumFields = "\t".join(["".join([j,"_",str(i+1)]) for i,j in enumerate(L)])		

	if __name__== "__main__":
		print NumFields
	return TotalFound,AllOut
	
def findassociation(conceptstrings,Debug=False):
	''' Specify two concepts or groups joined with + to see their associations'''
	
	
	if Debug:
		sys.stderr.write("Associating conceptstrings {}\n".format(conceptstrings))
	try:
		HostString,AssocString = conceptstrings.split("+")
		# HostList = HostString.split()
		# AssocList = AssocString.split()
	except ValueError:
		sys.exit("** To search for associations, provide two or more concepts separated by a plus\n")
	
	""" Make your own custom query """
	# print "HostString",  (getconceptfromKB([HostString]))
	# print "AssocString", AssocList
	Outstr=""
	TotalNum=0
	
	
	# This returns a tuple with number and string...
	HostCons = getconceptfromKB(HostString)[1]
	AssocCons = getconceptfromKB(AssocString)[1]
	sys.stderr.write("Finding associations for {0}... \n                    WITH {1}...\n".format(HostCons[:100],AssocCons[:100]))
	
	# Need to search both ways: fish annotated with jelly and jelly annotated with fish...
	for Flipper in ([HostCons,AssocCons],[AssocCons,HostCons]):
		FromCon = """ ann.ConceptName IN ({}) """.format(Flipper[0])
		# ToCon = """ ann.ToConcept IN ({},'Marine organism') """.format(Flipper[1])
		ToCon = """ ( (ann.ToConcept IN ({0}) ) OR (REPLACE(ann.Associations,'-',' ' ) IN ({0}) ) ) """.format(Flipper[1])
		# FromCon = """ ann.ConceptName IN ({}) """.format("'Chrysaora'")
		# ToCon = """ ann.ToConcept IN ({}) """.format("'Doryteuthis opalescens'")
		# Take all args as a string, split on commas (genus species have spaces)
	
		Aquery = """ SELECT
		ann.RovName, ann.DiveNumber,ann.DEPTH, ann.ConceptName,ann.Latitude, ann.Longitude, ann.Temperature, ann.Oxygen, ann.Salinity,
		ann.RecordedDate, ann.Image, ann.TapeTimeCode, ann.Zoom,
		ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
		ann.VideoArchiveSetID_FK AS vasid, ann.ShipName,
		ann.Associations,ann.videoArchiveName, ann.CameraDirection,
		ann.ChiefScientist, ann.FieldWidth, REPLACE(ann.Notes,CHAR(10),' '),
		ann.Observer, ann.ToConcept
		FROM Annotations AS ann
		WHERE (
		( ({0}) AND ({1}) ) 
		AND ( ann.linkName LIKE '%association%')   
		) """.format(FromCon,ToCon)
		if Debug:
			print >>sys.stderr, "query: ",Aquery
		# AND (( ann.linkName LIKE '%association%') OR (ann.linkName LIKE '%commensal%'))  

		NumFound,ResultStr = execquery(Aquery)

		TotalNum += NumFound 
		Outstr += ResultStr

	Fields = """RovName\tDiveNumber\tDepth\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\t\
	RecordedDate\tImage\tTapeTimeCode\tZoom\tObservationID\tLinkValue\tlinkName\t\
	VideoArchiveSetID\tShipName\tAssociations\tvideoArchiveName\tCameraDirection\t\
	ChiefScientist\tFieldWidth\tNotes\tObserver\tAssocConcept\n"""
	L = Fields.split("\t")
	while '' in L:
			L.remove('')
	NumFields = "\t".join(["".join([j,"_",str(i+1)]) for i,j in enumerate(L)])
	
	# head = """RovName	DiveNumber	Depth	ConceptName	Latitude	Longitude	Temperature	Oxygen	EpochSecs	Salinity	RecordedDate	Image	TapeTimeCode	Zoom	ObservationID	LinkValue	linkName	VideoArchiveSetID	ShipName	Associations	videoArchiveName	CameraDirection	ChiefScientist	FieldWidth	Light	Notes	Observer	AssocConcept"""
	# print Fields
	# sys.stderr.write("## Found %d associations for %s...\n" % (TotalNum,conceptstrings))

	return TotalNum, NumFields + Outstr

	
def getsamples(DiveList='',PIList='',Sample='',PreList=False,Debug=False,Super=False):
	# PreList is whether you are passing a list of [veh,name] strings or a string like v3762
	# removed Image, but add back in if you want Image URL
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	# DiveList = DiveListAsString.split(",")
	SQuery = """
	SELECT DISTINCT
	      CONVERT(varchar(22), RecordedDate, 120) as DateTime24,
	      RovName, DiveNumber, ConceptName, 
	      Depth, Temperature, Latitude, Longitude, TapeTimeCode, ISNULL(Observer, '') AS Observer,
	      LinkValue
	FROM  dbo.Annotations
	WHERE ((RovName like '{0}') AND (DiveNumber IN ('{1}') )
	AND ((LinkName LIKE '%sample-reference%') {3}) {2})
	ORDER BY DateTime24,TapeTimeCode ; """
	#sys.stderr.write("Finding samples...\n")
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		
		PreList = True
	elif not PreList:
		if Debug:
			print >> sys.stderr,"Before DiveList",DiveList
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList

	DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
	
	if Debug:
		print >> sys.stderr, DiveDict	
	if Sample:
		if "," in Sample:
			SampleString = "AND ConceptName IN ({})".format(Sample)
		else:
			SampleString = "AND (ConceptName LIKE '{}') ".format(Sample.lower())
	else:
		SampleString = ""
	SuperString = ""
	if Super:
		SuperString = " OR (LinkName LIKE '%%sampled%')"
	SHead = "DateTime24	RovName	DiveNumber	ConceptName	Depth	Temperature	Latitude	Longitude	TapeTimeCode	Observer	SampleRefName"
	print SHead
	
	for ROV in DiveDict:
		DiveNums =  "','".join(DiveDict[ROV])			
		sys.stderr.write("## Getting samples from {0} '{1}'...\n".format(ROV,DiveNums))
		SQ = SQuery.format(ROV, DiveNums, SampleString, SuperString)
		if Debug:
			print >> sys.stderr, "## Query:", SQ
		NumFound, Outstr = execquery(SQ, mydatabase="VARS")
		TotalFound += NumFound
		AllOut += Outstr
	sys.stderr.write("## Found %d samples for the query...\n" % (TotalFound))
	
	if TotalFound == 0 and not ('%' in SampleString):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	return AllOut

def getconceptbydive(DiveList='',PIList='',Sample='',PreList=False,Debug=False):
	# PreList is whether you are passing a list of [veh,name] strings or a string like v3762
	# removed Image, but add back in if you want Image URL
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	# DiveList = DiveListAsString.split(",")
	SQuery = """
	SELECT DISTINCT ann.RovName, ann.DiveNumber,ann.Depth, ann.ConceptName,ann.Latitude, ann.Longitude, ann.Temperature, ann.Oxygen, ann.Salinity,
	ann.RecordedDate AS DateTime24, ann.Image, ann.TapeTimeCode, ann.Zoom,
	ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
	ann.VideoArchiveSetID_FK AS vasid, ann.ShipName, ann.Associations,
	ann.videoArchiveName, ann.CameraDirection,
	ann.ChiefScientist, ann.FieldWidth, Replace(Notes,CHAR(10),' '),
	ann.Observer,ann.ToConcept
	FROM Annotations as ann
	WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
	{2}
	ORDER BY DateTime24,TapeTimeCode ; """

	## Old query
	# SQuery = """
	# SELECT DISTINCT
	#       CONVERT(varchar(22), RecordedDate, 120) as DateTime24,
	#       RovName, DiveNumber, ConceptName,
	#       Depth, Latitude, Longitude, TapeTimeCode, ISNULL(Observer, '') AS Observer,
	#       LinkValue
	# FROM  dbo.Annotations
	# WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
	# {2}
	# ORDER BY DateTime24,TapeTimeCode ; """

	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		PreList = True
	elif not PreList:
		if "," in DiveList:
			DiveList = DiveList.split(",") 
		else:
		 	DiveList = [DiveList]
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList

	DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList)
	
	if Debug:
		print >> sys.stderr, DiveDict	
	if Sample:
		if "," in Sample:
			SampleString = "AND ConceptName IN ({})".format(Sample)
		else:
			SampleString = "AND (ConceptName LIKE '{}') ".format(Sample.strip("'").lower())
	else:
		SampleString = ""
	
	
	Fields = """RovName\tDiveNumber\tDepth\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\t\
	RecordedDate\tImage\tTapeTimeCode\tZoom\tObservationID\tLinkValue\tLinkName\t\
	VideoArchiveSetID\tShipName\tAssociations\tvideoArchiveName\tCameraDirection\t\
	ChiefScientist\tFieldWidth\tNotes\tObserver\tToConcept"""
	
	# SHead = "DateTime24	RovName	DiveNumber	ConceptName	Depth	Latitude	Longitude	TapeTimeCode	Observer	Link"
	L = Fields.split("\t")
	while '' in L:
		L.remove('')
	NumFields = "\t".join(["".join([j,"_",str(i+1)]) for i,j in enumerate(L)])		

	# THIS IS THE HEADER...
	print NumFields
	
	for ROV in DiveDict:
		DiveNums =  "','".join(DiveDict[ROV])			
		sys.stderr.write("## Getting concepts from {0} '{1}'...\n".format(ROV,DiveNums))
		SQ = SQuery.format(ROV, DiveNums, SampleString)
		if Opt.DEBUG:
			print >> sys.stderr, "## Query:", SQ
		NumFound, Outstr = execquery(SQ, mydatabase="VARS",hurry=Opt.Hurry)
		TotalFound += NumFound
		AllOut += Outstr
	sys.stderr.write("## Found %d concepts for the query...\n" % (TotalFound))
	if TotalFound == 0 and not ('%' in SampleString):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	return AllOut
	
	
def parsediveoptions(DiveList='',PIList=''):
	PreList = False
	if PIList:
		DiveList = getdivebypi(PIList,ReturnThem=True)
		PreList = True
	elif "," in DiveList:
		DiveList = DiveList.split(",") 
	else:
		DiveList = [DiveList]
	DiveDict = divelisttodict(DiveList=DiveList,PreList=PreList,Short=True)
	return DiveDict
	
	
def getdivesummary(DiveList='',PIList='',Debug=False, PrintResult=True,UseShip=False):
	# removed Image, but add back in if you want Image URL
	# for epoch secs add this back in: DateDiff(ss, '01/01/70', RecordedDate) AS Esecs,
	# requires shortnames?
	
	# THIS VERSION USES AVGROVLAT -- NOT RELIABLE

	SQuery = """
	SELECT DISTINCT
		  chiefscientist, shipname, rovname,  divenumber, avgrovlat, avgrovlon, divestartdtg, maxpressure
	FROM  dbo.DiveSummary
	WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
	"""
	if UseShip:
		SQuery = """
		SELECT DISTINCT
			  chiefscientist, shipname, rovname,  divenumber, avgshiplat, avgshiplon, divestartdtg, maxpressure
		FROM  dbo.DiveSummary
		WHERE (RovName like '{0}') AND (DiveNumber IN ('{1}') )
		"""
	SHead = "ChiefScientist	ShipName	RovName	DiveNumber	AvgROVLatitude	AvgROVLongitude	DiveStartTime	MaxPressure"
	
	DiveDict = parsediveoptions(DiveList=DiveList,PIList=PIList)
	
	NumFound = 0
	TotalFound = 0
	AllOut = ""
	if Debug:
		print >> sys.stderr, "## DiveList:", DiveList


	if PrintResult:
		print SHead
	NumFound = 0
	TotalFound = 0
	AllOut = ''
	for ROV in DiveDict:
		DiveNums =  "','".join(DiveDict[ROV])		
		SQ = SQuery.format(ROV,DiveNums)	
		if Debug:
			print SQ
		NumFound,Outstr = execquery(query = SQ, mydatabase="EXPD")
		TotalFound +=NumFound
		AllOut += Outstr
	if PrintResult:
		sys.stderr.write("## Found %s dive summaries...\n" % (TotalFound))
	return AllOut


def findconcept(conceptstrings,wild=False,hurry=False):
	if wild:
		ConString = """ ann.ConceptName like '%%%s%%' """
	else:
		ConString = """ ann.ConceptName like '%s' """
	# Take all args as a string, split on commas (genus species have spaces)
	if "," in conceptstrings:
		Splits = conceptstrings.split(",")
		ConList = [ConString % (Con.strip()) for Con in Splits]
		Constraint = "( %s )" % (" OR ".join(ConList))
	else:
		ConList = []
		if conceptstrings:
			Constraint = ConString % (conceptstrings)
		else:
			Constraint = ''
	# Concept = " ".join(sys.argv[1:])
	if Opt.DEBUG:
		print >> sys.stderr, Constraint
	query = """ SELECT
	ann.RovName, ann.DiveNumber,ann.DEPTH, ann.ConceptName,ann.Latitude, ann.Longitude, ann.Temperature, ann.Oxygen, ann.Salinity,
	ann.RecordedDate, ann.Image, ann.TapeTimeCode, ann.Zoom,
	ann.ObservationID_FK AS obsid, ann.LinkValue, ann.linkName,
	ann.VideoArchiveSetID_FK AS vasid, ann.ShipName, ann.Associations,
	ann.videoArchiveName, ann.CameraDirection,
	ann.ChiefScientist, ann.FieldWidth, Replace(Notes,CHAR(10),' '),
	ann.Observer,ann.ToConcept
	FROM
	Annotations AS ann
	where
	%s """ % Constraint

	Fields = """RovName\tDiveNumber\tDepth\tConceptName\tLatitude\tLongitude\tTemperature\tOxygen\tSalinity\t\
	RecordedDate\tImage\tTapeTimeCode\tZoom\tObservationID\tLinkValue\tLinkName\t\
	VideoArchiveSetID\tShipName\tAssociations\tvideoArchiveName\tCameraDirection\t\
	ChiefScientist\tFieldWidth\tNotes\tObserver\tToConcept"""

	L = Fields.split("\t")
	# Remove blank fields (e.g. at the end) -- probably not needed?
	while '' in L:
			L.remove('')
	NumFields = "\t".join(["".join([j,"_",str(i+1)]) for i,j in enumerate(L)])		
	# NumFields = "\t".join(["".join([str(i+1),j]) for i,j in enumerate(L)])
	sys.stderr.write("Finding all annotations for %s...\n" % conceptstrings)
	# head = """RovName	DiveNumber	Depth	ConceptName	Latitude	Longitude	Temperature	Oxygen	EpochSecs	Salinity	RecordedDate	Image	TapeTimeCode	Zoom	ObservationID_FK	LinkValue	linkName	VideoArchiveSetID_FK	ShipName	Associations	videoArchiveName	CameraDirection	ChiefScientist	FieldWidth	Light	Notes	Observer	ToConcept"""
	print NumFields
	if Opt.DEBUG:
		sys.stderr.write("QUERY:\n%s\n" % query)
		
	NumFound,Outstr = execquery(query,hurry=hurry)
	sys.stdout.write(Outstr)
	sys.stderr.write("## Found %d annotations for %s...\n" % (NumFound,conceptstrings))
	if NumFound == 0 and not ('%' in conceptstrings):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	
	if ConList:
		return ConList

def checktransect():
	'''TBD: either check if a list of dive numbers is transects, or return transect status for a list of dives'''
	
	TQuery = '''SELECT DISTINCT RovName, DiveNumber, CameraDirection, ChiefScientist
	FROM Annotations WHERE (  (CameraDirection LIKE 'transect%' )  AND (ChiefScientist LIKE '%Robison%') )
	ORDER BY RovName, DiveNumber;'''



def getimages(listOnly=False,getConcept=False,dive=False,png=False,wild=False,comment=''):
	
	if comment:
		CommentString = "AND ((LinkName Like '%{comment}%' ) OR ( LinkValue LIKE '%{comment}%' ) or (ToConcept LIKE '%{comment}%') )".format(comment=comment)
	else:
		CommentString = ""
		
	DiveJoin = ""
	# TODO  Need to fix for multiple dives
	
	
	# if dive:
	# 	if  "," in dive:
	# 		DiveList = dive.split(",")
	# 	else:
	# 		DiveList = [dive]
	Constraint = ""
	if dive:
		Veh,Num,Sam = parsedivenumbers(dive)
		Constraint = """ (ann.RovName like '%%{0}%%') AND (ann.DiveNumber = {1}) {2}""".format(Veh,Num,CommentString)
		DiveJoin = "AND "
	
	Splits = []
	
	if getConcept:
		if wild:
			ConString = """ ann.ConceptName like '%%%s%%' """
		else:
			ConString = """ ann.ConceptName like '%s' """
		if "," in getConcept:
			Splits = getConcept.split(",")
			ConList = [ConString % (Con.strip().replace("'","")) for Con in Splits]
			Con = "( %s )" % (" OR ".join(ConList))
		else:
			Con = ConString % (getConcept)

		Constraint += DiveJoin + Con
		# Concept = " ".join(sys.argv[1:])
		if Opt.DEBUG: 
			print >> sys.stderr, "Constraint: ",Constraint
		# instead of limit use TOP 100
	
	query = """ SELECT 
	ann.RovName, ann.DiveNumber,ann.Image, ann.ConceptName
	FROM
	Annotations AS ann
	where
	{} {}""".format(Constraint,CommentString)
	
	if Opt.DEBUG:
		print >> sys.stderr, query
	Fields = """Vehicle\tDiveNumber\tImage\tConcept"""

	if not listOnly:
		sys.stderr.write("Finding all images for Concept:%s and Dives:%s...\n" % (getConcept,dive))
	
	NumFound,Outstr = execquery(query)
	imagecount=0
	#TODO: Image count not working?
	for S in Outstr.split('\n'):
		if '\t' in S and not ((S.split('\t')[-2] == 'None') or (S.split('\t')[-2] == '')):
			imagecount+=1
			if listOnly:
				print S
			else:
				## 
				Vehicle,Dive,URL,Con2 = S.split("\t")
				if png:
					URL = URL.replace(".jpg",'.png').replace('.JPG','.PNG')
				FileTag = Vehicle[0]+Dive
				Fname = URL.split('/')[-1]
				URLtext = "curl -s '{0}' > '{1}-{2}-{3}'".format(URL,Con2.replace(" ","_"),FileTag,Fname)
				if len(Fname) > 7:
					if Opt.DEBUG:
						print >> sys.stderr, URLtext
					ProcOut = subprocess.Popen(URLtext, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,shell=True)
					ProcOut.wait()
					sys.stderr.write(".")
	
	if not listOnly:
		try:
			divestr = "on "+ Veh[0]+"-"+Num
		except NameError:
			divestr = ""
		sys.stderr.write("\n## Found %d images in %d annotations for %s %s\n" % (imagecount,NumFound,getConcept,divestr))
	if NumFound == 0 and not listOnly and not ('%' in Con):
		sys.stderr.write("If unexpected zero results, make sure to use -k for genus only searches\n")
	
def getbiolum(getConcept='',style=0):
	
	ConString = "cn.ConceptName like '%s'"
	Splits = []
	if getConcept:
		if "," in getConcept:
			Splits = getConcept.split(",")
			ConList = [ConString % (Con.strip().replace("'","")) for Con in Splits]
			Con = "AND ( %s )" % (" OR ".join(ConList))
		else:
			Con = "AND " + ConString % (getConcept)
			Splits = [getConcept]
	else:   # GET FULL LIST
		Con = ''
	
	
	BQ = '''SELECT cn.ConceptName, lr.LinkValue AS link
	FROM
	 Concept AS c LEFT JOIN
	 ConceptName AS cn ON cn.ConceptID_FK = c.id LEFT JOIN
	 ConceptDelegate AS cd ON cd.ConceptID_FK = c.id LEFT JOIN
	 LinkRealization AS lr ON lr.ConceptDelegateID_FK = cd.id 
	WHERE LinkName = 'is-bioluminescent' {constring}
	ORDER BY cn.ConceptName'''.format(constring=Con)

	if Opt.DEBUG:
		print >> sys.stderr, BQ
	n,outlist = execquery(BQ,mydatabase="VARS_KB",keeplist=True)
	
	
	if style:
		print "BiolumDict = {"
		for con,bio in outlist:
			print "'{}' : '{}',".format(con.replace("'","\\'"),bio)
		print "'NA':'non-bioluminescent' }  # End BiolumDict"
		
	else:
		if Splits:  # Concepts were provided to search on
			cd = dict(outlist)
			for rawcon in Splits:
				con = rawcon.replace("'","").strip()
				print "{}\t{}".format(con,cd.get(con,"NA"))
		else:# No concepts, so returning whole database
			for con,bio in outlist:
				print "{}\t{}".format(con,bio)


def summarizeassoc(instring):
	""" Split the associations to a subset of pairs"""
	""" DID something change? This had an error"""

	allstr = instring.split("\n")[1:]
	OutSet = {}
	for Line in allstr:
		Values = Line.split("\t")
		if len(Values)>25:
			OutKey = Values[3].replace("Hyperiidea","Hyperiidae") + " + " + Values[25].replace("Hyperiidea","Hyperiidae")
			OutSet[OutKey] = OutSet.get(OutKey,0) + 1
	OK = sorted(OutSet.keys())
	for k in OK:
		print "%8d  %s" % (OutSet[k],k)

	



### START OF PROGRAM
	
def main():
	
	CarryOn = True
	CommentString = ""
	if len(sys.argv) <2:
		sys.exit(__doc__)
	if Opt.Biolum:
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
		getbiolum(Opt.Concept,style=Opt.Format)
		CarryOn=False
	if Opt.Anela:
		style = Opt.Format
		assocnum, assocstr = allassociations(DiveList=Opt.Dives,PIList=Opt.Pis,PreList=False,Style=Opt.Format,Debug=Opt.DEBUG)
		if style:
			print assocstr
		else:
			summarizeassoc(assocstr)
		sys.stderr.write("\n## Found %d feeding annotations...\n" % (assocnum ))
		CarryOn=False
	if Opt.Normalize:
		DepthDictionary = generatenormalization(DiveList=Opt.Dives,PIList=Opt.Pis,BinSize=10)
		Dkeys = DepthDictionary.keys()
		Dkeys.sort()
		print"Depth\tSeconds"
		for D in Dkeys:
			print "{}\t{}".format(D,DepthDictionary.get(D))
		CarryOn=False
	elif Opt.Comment and not Opt.GetImages:
		if not (Opt.Concept) and not(Opt.Knowledge):
			sys.exit("Comment searches require a concept -c or Category -k also.")
		
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
		if Opt.DEBUG:
			print >> sys.stderr, "CONCEPT",Opt.Concept
		if not Opt.GetImages:
			assocnum, assocstr = findwithcomment(Comment=Opt.Comment,Concept=Opt.Concept,DiveList=Opt.Dives,PIList=Opt.Pis,PreList=False,Style=Opt.Format,Debug=Opt.DEBUG)
			print assocstr
			sys.stderr.write("\n## Found %d annotations for '%s' with comment '%s'...\n" % (assocnum,Opt.Concept,Opt.Comment))
			CarryOn=False
	elif Opt.UpConcept:
		style=int(Opt.Format)
		print getparentconcepts(Opt.UpConcept,style=style) + "," + Opt.UpConcept
	elif Opt.GetDepth: # flawed?
		print getrovdepth(Opt.Args[0])
		CarryOn=False
	elif Opt.GetPNG:
		Opt.GetImages = True
	elif Opt.Samples or Opt.Supersample:
		if not Opt.Dives:
			if Opt.Args:
				Opt.Dives = ",".join(Opt.Args)
				print >> sys.stderr, "DiveArgs",Opt.Dives
		elif Opt.Args:
			Opt.Dives = Opt.Dives + ","+",".join(Opt.Args)
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
			if Opt.DEBUG:
				print >> sys.stderr, Opt.Concept
		print(getsamples(DiveList=Opt.Dives, PIList=Opt.Pis, Sample=Opt.Concept, PreList=False, Debug=Opt.DEBUG,Super=Opt.Supersample))
		CarryOn = False
	elif (Opt.ListImages or Opt.GetImages):
		# FIX THIS wrt IMAGES...!!!!!!
		Strings=""
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
		if Opt.Dives or Opt.Pis:
			if Opt.Pis:
				DiveList = getdivebypi(Opt.Pis,ReturnThem=True)
				DiveStringList = [A[0].upper() + B for A,B in DiveList]
				if Opt.DEBUG:
					print >> sys.stderr, "DiveListFromPi",DiveStringList
				Opt.Dives = ",".join(DiveStringList)
			if Opt.Args:
				Strings = Opt.Dives + ","+",".join(Opt.Args)
			else:
				Strings = Opt.Dives
			if Opt.DEBUG:
				print >> sys.stderr, "Divestring:",Strings
			for D in Strings.split(","):
				getimages(listOnly=Opt.ListImages,getConcept=Opt.Concept,dive=D,png=Opt.GetPNG,wild=Opt.Wildcard,comment=Opt.Comment)
		else:
			getimages(listOnly=Opt.ListImages,getConcept=Opt.Concept,dive=Opt.Dives,png=Opt.GetPNG,wild=Opt.Wildcard,comment=Opt.Comment)
		CarryOn = False
	elif Opt.Pis and CarryOn:
		if not (Opt.Knowledge or Opt.Concept):
			getdivebypi(Opt.Pis) # return for internal script use, or print (default)
		else:
			DiveList = getdivebypi(Opt.Pis,ReturnThem=True)
			DiveStringList = [A[0].upper() + B for A,B in DiveList]
			if Opt.DEBUG:
				print >> sys.stderr, "DiveListFromPi",DiveStringList
			Opt.Dives = ",".join(DiveStringList)
			CarryOn = True
	if CarryOn and Opt.Dives:
		if Opt.Args:
			Strings = Opt.Dives + ","+",".join(Opt.Args)
		else:
			Strings = Opt.Dives
		if Opt.Knowledge:
			Opt.Concept = getconceptfromKB(Opt.Knowledge)[1]
		if Opt.ListImages or Opt.GetImages:
			getimages(listOnly=Opt.ListImages,getConcept=Opt.Concept,dive=Opt.Dives,png=Opt.GetPNG)	
		elif (Opt.Concept):
			print(getconceptbydive(DiveList=Strings, PIList=Opt.Pis, Sample=Opt.Concept, PreList=False, Debug=Opt.DEBUG))
		else:
			print getdivesummary(Strings,UseShip = Opt.useship).rstrip("\n")
		if Opt.DEBUG:
			print >> sys.stderr, "D Strings:",Strings
			print >> sys.stderr, "Opt.Args:",Opt.Args
		CarryOn = False
	elif CarryOn and Opt.Knowledge:
		style=int(Opt.Format)
		if Opt.Args:
			Strings = " ".join([Opt.Knowledge,Opt.Args])
		else:
			Strings = Opt.Knowledge
		if Opt.DEBUG:
			print >> sys.stderr, "K Strings:",Strings
		print( getconceptfromKB(Strings, style=style)[1])
	elif CarryOn and Opt.Concept or Opt.Wildcard:
		if Opt.Wildcard:
			sys.stderr.write("** Wildcards turned ON. Searching inclusively.\n")
			if not Opt.Concept:
				Opt.Concept = Opt.Args[0]
		else:
			sys.stderr.write("** Wildcards turned OFF. Searching strictly. Use %% to override.\n")
		findconcept(conceptstrings=Opt.Concept,wild=Opt.Wildcard,hurry=Opt.Hurry)
		if len(Opt.Args) > 1:
			sys.stderr.write("** NOTE: Concepts should be comma-separated or surrounded in quotes.\n Space-separated lists will not work as expected\n")
		
	elif Opt.Association:
		style = Opt.Format
		assocnum, assocstr = findassociation(Opt.Association,Opt.DEBUG) 
		sys.stderr.write("\n## Found %d annotations for %s (and vice versa)...\n" % (assocnum,Opt.Association ))
		if style:
			print assocstr
		else:
			summarizeassoc(assocstr)





if __name__ == "__main__":
	try:
		import argparse
	except ImportError:
		sys.exit("Argparse (standard in python 2.7+) is required.\n Consider updating python to 2.7?")
	import sys
	import subprocess
	import os
	import time
	try:
		import pymssql
	except ImportError:
		sys.exit(InstallString)
		
	Opt = get_options()

	_,Onsite = getmyip2(Opt.DEBUG)

	if not Onsite:
		sys.stderr.write("# Offsite detected. Limited options\n")

	if Opt.DEBUG:
		print >> sys.stderr, "OPTIONS: ",Opt
	
	main()


"""
DiveSummary in EXPD has the following fields:
COLUMN_NAME,avgrovlat,avgrovlon,avgshiplat,avgshiplon,chiefscientist,
ctdconfigid,ctdlastupdate,ctdlastwho,ctdlastwhy,ctdo2optodecount,ctdo2sbecount,
ctdpcount,ctdplotsent,ctdscount,ctdtcount,ctdxmisscount,diveenddtg,diveid,
divenumber,divestartdtg,expdid,id,maxpressure,maxrovlat,maxrovlon,maxshiplat,
maxshiplon,minrovlat,minrovlon,minshiplat,minshiplon,rovid,rovname,
shipid,shipname
"""

