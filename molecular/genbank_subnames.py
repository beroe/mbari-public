#!/usr/bin/env python

# This script retrieves a fasta file for each accession number you enter on the query, can be protein or
# nucleotide sequences. It will also search for the header string by using regular expression: r"^>gi\|(\d+).*\[(.*)\]"
# And the each sequence will be have only the name of the specie and the GI number separated by _
# For help on curl format, see the eutils help here:
# http://www.ncbi.nlm.nih.gov/entrez/query/static/esummary_help.html#DisplayNumbers

import os
import re

sentence = "--------------------- "
								    

query="XP_002507516.1,XP_003084401.1,EFN53469.1,CBJ33604.1,XP_003062934.1,CBJ33346.1,CBJ26089.1,XP_001422370.1,XP_003063809.1,EFN54222.1,NP_176658.1,XP_002500242.1,XP_003061951.1,XP_001418520.1,XP_003082182.1,CBJ48742.1,CBJ33299.1,EGB12452.1,CBN77714.1,XP_002503565.1,XP_003055565.1,XP_001417910.1,ABZ10124.1,ABZ07271.1,ZP_07944338.1,NP_618789.1,YP_001114191.1,YP_002797235.1,YP_303722.1,YP_001672649.1,ZP_01224824.1,XP_003062934.1,XP_002508410.1,NP_189072.1,XP_002885635.1,XP_002885634.1,XP_002314518.1,NP_189073.1,XP_002875166.1,AAD54638.1,AAG28780.1,YP_429070.1,YP_003639472.1,YP_003702898.1"
query="XP_002507516.1,XP_003084401.1,EFN53469.1"
query="NZ_AASF01000001,NZ_AASF01002170"

query=["NZ_AASF0100%04d" % (x) for x in range(1,2171)]
#print mquery
mydatabase="protein"
mydatabase="nucleotide"

retmax=query.count(',')+1

SearchStr=r"^>gi\|(\d+).*\[(.*)\]"
#  for fasta
for Accession in query.split(","):
#	retcurl='curl -s "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?tool=quicktest&db='+mydatabase+"&retmax="+str(retmax)+'&retmode=text&rettype=fasta&id='+ Accession +'"'
	retcurl='curl -s "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?tool=quicktest&db='+mydatabase+"&retmax="+str(retmax)+'&retmode=text&rettype=gbwithparts&id='+ Accession +'"'
	#print retcurl
	stdout_handle = os.popen(retcurl,'r')
	# print retcurl
	temphold=stdout_handle.read().rstrip()
	
	NewOut = temphold
	#NewOut=re.sub(SearchStr,r">\2_\1",temphold).replace(" ","_")
	#Results=re.search(SearchStr,temphold)
	
	#print "GI: ", Results.group(1)
	#print "Species: ", Results.group(2)
	print NewOut
	
