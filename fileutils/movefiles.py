#! /usr/bin/env python

"""
finds all <Extension> files in all immediate subdirectories 
of the current directory and renames them by adding the
directory name to the front, moving them to the 
<Destination> folder.

Must be run in a Unix or mac-like environment
"""

import os

Destination = 'renamed'
Extension="JPG"
DirList = os.popen('ls -F | grep \/','r').read().split()

# print DirList.strip()
for Direct in DirList:
	print "Directory", Direct,'-----------------'
	FileList=os.popen('ls ' + Direct +'*.'+Extension).read().split()

	if len(FileList)==0:
		print "No files found in",Direct
	else:
#		print "yes", FileList
		for PathName in FileList:
			FileName=PathName.split('/')[1]
			if FileName[0]==('_'):
				FileName=FileName[1:]
			#print FileName
			NewName='../'+Destination+'/'+Direct[:-1]+'_'+FileName
			command = 'cp ' + PathName + " " + NewName
			print command
			os.popen(command,'r')
		os.popen('chmod 644 ../' + Destination + '/*.'+ Extension ,'r')
		

