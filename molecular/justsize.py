#! /usr/bin/env python


usage="""
JUSTSIZE.PY
  Print out the sizes of the sequences in a file
  No names are included, but could be easily added
  Useful for generating histograms

  Requires the mybio.py mini-library
  
Usage: 
	justsize.py Tr100_filename.fasta > lengths.txt
	
"""
import sys
import mybio as mb

if len(sys.argv)<2:
	print usage	

else:
	inputfilename = sys.argv[1]
	orderedlist,sequences = mb.readfasta_nocheck(inputfilename,False,False,True) 
	
	for sname in orderedlist:
		print len(sequences[sname])



