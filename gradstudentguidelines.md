# Some tips for Haddock Lab Grad Students

## Interpersonal dynamics

### Meshing with your peers

The grad student population is drawn from people who were the smartest go-getterest people in their schools. That is why you were selected and why you are here. Awesome! The problem is that at times students feel they have to make sure that others around them know how smart they are. you do not need to prove to anyone that you belong. The best approach is pretty much always to let your work demonstrate how good of a scientist you are.  

Everyone has a different background, and different skills and interests. There are countless opportunities to LEARN from your peers, so have an open mind and a humble heart because these are the people who you will grow with, and you will probably work with them for years to come.
        
### Working well with others

It goes without saying that the lab technician is an integral part of the lab, and like all members should be treated with utmost respect and cordiality.

The lab technician is not your maid. She outranks you greatly, and although the PI might ask her to do lab "chores", this does not mean that she is there to make media, clean your dishes, pour plates, load your gear on the ship, or run your PCR. On the contrary, it would be wise for you to do many of those tasks for *her*, so that when you do need assistance, she is motivated to reciprocate for your good citizenship.
        
Similarly, the administrative and support staff -- even the janitors -- have seniority, and should always be treated with respect. If they do something to help you out, it is not because this is their job, but rather that they are being exceedingly nice.

### Dating

Try to use your scientific mind as much as possible here, and don't do anything in the workplace (or ships) you would not want captured on a security camera or posted on social media! Think about if things don't work out, will one of you have to leave the program? Be aware of balance of power issues (TA/student/intern) and competition. If it really is true love, then it should be able to survive until you don't overlap in either work-related or supervisorial contexts.

## Lab safety

You will receive formal lab safety training, but there is also room for developing general habits that promote lab safety. So many times when you have an "oops" moment (splash of reagent in the eye, broken glassware) you know that you shouldn't have been doing what you were doing. There is no Undo button in real life, and try not to risk a low-probability hazard, just to avoid pausing to track down your safety goggles or to assess your surroundings and you next steps.

There have been some fatal and disfiguring accidents recently (UCLA and University of Hawaii) where people were doing tasks that had become routine to them. Even being in a closed space with liquid nitrogen or dry ice can be hazardous. Notify a colleague, use appropriate protective gear, and don't take risks. 

## Financial considerations

As a grad student, you will be paid a certain amount, and not strictly an hourly wage. This applies whether you are charging to the grant, paid by the University while TAing, or on a departmental fellowship. Your payment is a stipend, which is supposed to allow you to get your education and degree without taking additional jobs. As my advisor used to say, "You get paid just enough to keep you stocked with fish heads and rice." This is not a money-making job, but is more comparable to medical school or law school, though you don't accrue so much debt.

For accounting reasons that are unclear to me, your time card will say that you are being paid "half time". Until you get used to it, reporting only 20 hours per week is going to sound unfair, since you might work that long in a single day on occasion! Don't think of it as an hourly wage, but just consider that you are being paid a flat rate stipend for your time. We all went through this.

Another important thing to note is that the nominal 20-hours per week does not mean that you can charge another 20 hours to a different project. For example, you would not charge 20 hours to the grant while being paid for 20 hours/week TAing. 

Even when we are at sea 24 hours a day for a solid week, *you still charge only 20 hours*. The only difference is that when we are out on multiday trips (not on single-day trips) you will charge your 20 hours to the special SeaPay subcategory, if supported on a grant during that time. Hours charged to SeaPay accrue special paid leave hours.

If you are having financial difficulties, or are confused about proper procedures, don't hesitate to let me know about it, or if you prefer, talk to someone from human resources. 

*This goes for all other lab issues too. We can't make it better if we don't know what is going on. I don't like to be surprised because you thought it was better to ask forgiveness than permission.*  
       
## Scientific integrity

Admonitions against cheating are at the heart of every code of conduct, but everyone already knows not to cheat. You are much more likely to be faced with choices of a more subtle nature. Should you throw out that failed trial and re-run once more? Do you really need to track down that paper from the 1950s, or can you trust that recent paper which cited it, and said it supported your claims? Being puritanical about these subtle choices that you have to make each day will help ensure that you have a long and productive career. Short-term glory at the expense of rigorous science will not endure, since the truth comes out in the end.

## TO DO: AUTHORSHIP
All authors must have a chance to read paper before submission.
Include all people who have contributed to the project. 

## Mentorship style
By the time you are in grad school, I expect you to be an independent scientific thinker. This lab does not operate where each student is a cog in a wheel, tackling one facet of a predetermined research topic. We are funded to do certain tasks, but that umbrella casts a fairly broad shadow. You should think about what you want to be know for, scientifically, and what will be your field of expertise and specialization.

Try to think of experiments and projects that will target the large multi-year synthesis but also the quick opportunistic observational paper. There are lots of discoveries to be made, and some of your most interesting findings may be a two-page report in _Nature_.

We will have weekly lab meetings, but if you want to talk to me about anything -- scientifically, or logistically or just how your project is going in more detail -- you are welcome to pop in at ny time, or set up a time to meet.
        
## APPENDIX
## Rules for Knights (and women and men of grad school)

>Ethan Hawke wrote a short book laying out a few rules of conduct for knights. Many of them apply equally well to graduate students.

*  HUMILITY:  Never announce that you are a knight, simply behave as one. You are better than no one, and no one is better than you.

*  SOLITUDE:   Create time alone with yourself. When seeking the wisdom and clarity of your own mind, silence is a helpful tool.

*  SPEECH;   Do not speak ill of others. A knight does not spread news that he does not know to be certain, or condemn things that he does not understand.

*  FORGIVENESS:  Those who cannot easily forgive will not collect many friends. Look for the best in others.

*  GRACE:   Grace is the ability to accept change. Be open and supple; the brittle break.

*  PATIENCE;   There is no such thing as a once-in-a-lifetime opportunity. A hurried mind is an addled mind; it cannot see clearly or hear precisely; it sees what it wants to see, or hears what it is afraid to hear, and misses much. A knight makes time his ally. There is a moment for action, and with a clear mind that moment is obvious.

* DEDICATION;   Ordinary effort, ordinary result. Take steps each day to better follow these rules. Luck is the residue of design. Be steadfast. The anvil outlasts the hammer.

## LINKS

http://www.nature.com/naturejobs/science/articles/10.1038/nj7627-127a
http://www.nature.com/naturejobs/science/articles/10.1038/nj7574-597a
https://medium.com/@caseywdunn/applying-to-biology-phd-programs-58abece3284a#.qcs1j8k67
http://www.nature.com/naturejobs/science/articles/10.1038/nj7581-295a

