#!/usr/bin/env python

""" 
genbank_accession.py
This script retrieves a fasta file for each accession number you enter on the query, can be protein or
nucleotide sequences. 
If you uncomment a bit of code, it will also edit the header string by using regular expression: r"^>gi\|(\d+).*\[(.*)\]"
Each sequence will have only the name of the species and the GI number separated by _
For help on curl format, see the eutils help here:
http://www.ncbi.nlm.nih.gov/entrez/query/static/esummary_help.html#DisplayNumbers
"""
import os
import re
import sys


# Bulk!!
# query=["NZ_AASF0100%04d" % (x) for x in range(1,2171)]

#outgroups...
query = ["KC443057.1","AY660918.1","X55548.1","AB000599.1","EU982145.1","JN594054.1","JN594043.1","AY383572.1","HM369414.1","HQ417110.1","U31084.1","DQ665992.1","JX070082.1","JF837053.1","JF837061.1","AF519239","AF519270","AF519295","EF521186.1","EF521189.1","AF120511","AF120570","AY070145","AY940978.1","AY377729.1","U91490","U91492","AF370813","HM244865.1","AF370827"]

#print mquery
mydatabase="protein"
mydatabase="nucleotide"

retmax=query.count(',')+1

SearchStr=r"^>gi\|(\d+).*\[(.*)\]"
#  for fasta
count = 0
#RecordType = "gbwithparts"
RecordType = "fasta"

sys.stderr.write("Working on {0} queries...\n".format(len(query)))
for Accession in query:
#	retcurl='curl -s "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?tool=quicktest&db='+mydatabase+"&retmax="+str(retmax)+'&retmode=text&rettype=fasta&id='+ Accession +'"'
	sys.stderr.write(".")
	if (count>0) and not (count % 10):
		sys.stderr.write("+\n")
	count += 1
	retcurl ='curl -s "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?tool=quicktest&db='+mydatabase+"&retmax="+str(retmax)+'&retmode=text&rettype=' + RecordType +'&id='+ Accession +'"'
	#print retcurl
	stdout_handle = os.popen(retcurl,'r')
	# print retcurl
	temphold=stdout_handle.read().rstrip()
	
	NewOut = temphold
	#NewOut=re.sub(SearchStr,r">\2_\1",temphold).replace(" ","_")
	#Results=re.search(SearchStr,temphold)
	
	#print "GI: ", Results.group(1)
	#print "Species: ", Results.group(2)
	print NewOut
sys.stderr.write("\nDone...\n")

